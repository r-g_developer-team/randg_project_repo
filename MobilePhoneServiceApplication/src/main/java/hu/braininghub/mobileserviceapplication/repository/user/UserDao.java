/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.user;

import hu.braininghub.mobileserviceapplication.repository.CrudRepository;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;

import hu.braininghub.mobileserviceapplication.repository.entity.usergroupentity.UserGroup;

import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;

import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Leo
 */
@Slf4j
@Singleton
@LocalBean
public class UserDao implements CrudRepository<User, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<User> findAll() {
        return (List) (em.createQuery("SELECT a FROM User a").getResultList());
    }

    @Override
    public Optional<User> findById(Integer id) {
        try {
            User u = (User) em.createQuery("SELECT u FROM User u WHERE u.userId=:id")
                    .setParameter("id", id)
                    .getSingleResult();

            return Optional.of(u);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public Optional<User> findByEmail(String email) {
        try {
            User u = (User) em.createQuery("SELECT u FROM User u WHERE u.email=:email")
                    .setParameter("email", email)
                    .getSingleResult();
            log.info("find user by email: " + u.toString());

            return Optional.of(u);

        } catch (NoResultException e) {
            return Optional.empty();
        }

    }

    public Optional<User> findUserByWorksheetId(WorkSheet worksheet) {
        try {
            User u = (User) em.createQuery("SELECT u FROM User u WHERE :worksheet MEMBER OF u.workSheets")
                    .setParameter("worksheet", worksheet)
                    .getSingleResult();

            return Optional.of(u);

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public void setRole(String email, String role) {
        em.createQuery("UPDATE UserGroup u SET u.group = :inputGroup WHERE u.email = :inputEmail")
                .setParameter("inputGroup", role)
                .setParameter("inputEmail", email)
                .executeUpdate();
    }
    
    public void setEmailInUserGroup(String email, String origin){
        em.createQuery("UPDATE UserGroup u SET u.email =:inputEmail WHERE u.email = :originEmail")
                .setParameter("inputEmail", email)
                .setParameter("originEmail", origin)
                .executeUpdate();
    }

    @Override
    public void deleteById(Integer id) {
        User u = em.find(User.class, id);
        if (u != null) {
            em.remove(u);
        }
    }

    public void deleteUserGroup(String email) {
        em.createQuery("DELETE FROM UserGroup u WHERE u.email =:inputEmail")
                .setParameter("inputEmail", email)
                .executeUpdate();
    }

    public Boolean isAdmin(String email) {
        UserGroup u = (UserGroup) em.createQuery("SELECT u FROM UserGroup u WHERE u.email =:inputEmail")
                .setParameter("inputEmail", email)
                .getSingleResult();
        if ("admin".equals(u.getGroup())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;

    }

    /**
     * This method updates entity's password in data base. The entity has the
     * new password from dto 1. generate new pw -> set this new pw for dto. 2.
     * mapping dto to entity already with the new pw 3. this new entity have to
     * be given in this method's parameter
     */
    public void updatePassword(User entity) {
        String newPassword = entity.getPassword();
        String emailAddress = entity.getEmail();
        em.createQuery("UPDATE User u SET u.password = :inputPassword WHERE u.email = :inputEmail")
                .setParameter("inputPassword", newPassword)
                .setParameter("inputEmail", emailAddress)
                .executeUpdate();
    }
    
    @Override
    public void update(User entity) {
        em.merge(entity);
    }

    @Override
    public void save(User entity) {
        em.persist(entity);
    }

    public void save(UserGroup entity) {
        em.persist(entity);
    }

}
