/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository;

import java.util.Optional;

/**
 *
 * @author momate
 * @param <Entity>
 * @param <ID>
 */
public interface CrudRepository<Entity, ID> {

    Iterable<Entity> findAll();

    Optional<Entity> findById(ID id);

    void deleteById(ID id);

    void update(Entity entity);

    void save(Entity entity);
}
