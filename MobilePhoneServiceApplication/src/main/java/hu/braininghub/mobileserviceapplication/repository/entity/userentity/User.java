/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.entity.userentity;

import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Leo
 */

@Entity
@EqualsAndHashCode(of="userId")
@Table (name = "users")
public class User implements Serializable{
    
    static final long serialVersionUID = 2L;

    public User() {}

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
        
        
    }

    public User(Integer userId, String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
    }
    
    
    @Id
    @Getter
    @Setter
    @Column (name = "user_id")
    private Integer userId;
    
    @OneToMany(mappedBy = "userId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<WorkSheet> workSheets;
    
    @Getter
    @Setter
    @Column (name = "first_name")
    private String firstName;
    
    @Getter
    @Setter
    @Column (name = "last_name")
    private String lastName;

    @Getter
    @Setter
    @Column (name = "email")
    private String email;
    
    @Getter
    @Setter
    @Column (name = "phone_number")
    private String phoneNumber;
    
    @Getter
    @Setter
    @Column (name = "postal_code")
    private String postalCode;
    
    @Getter
    @Setter
    @Column (name = "city")
    private String city;
    
    @Getter
    @Setter
    @Column (name = "password")
    private String password;
}
