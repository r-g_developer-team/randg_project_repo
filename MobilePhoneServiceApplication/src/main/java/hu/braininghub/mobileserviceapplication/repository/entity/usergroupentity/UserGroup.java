/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.entity.usergroupentity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author momate
 */

@Data
@Entity
@Table(name = "usergroup")
public class UserGroup implements Serializable {

    static final long serialVersionUID = 2L;

    @Id
    @Column(name = "groupName")
    private String group;
    @Column(name = "email")
    private String email;

    public UserGroup() {
    }

    public UserGroup(String group, String email) {
        this.group = group;
        this.email = email;
    }

}
