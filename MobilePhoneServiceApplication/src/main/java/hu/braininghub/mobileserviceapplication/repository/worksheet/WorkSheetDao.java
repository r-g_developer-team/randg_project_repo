/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.worksheet;

import hu.braininghub.mobileserviceapplication.repository.CrudRepository;
import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Leo
 */
@Singleton
@LocalBean
public class WorkSheetDao implements CrudRepository<WorkSheet, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<WorkSheet> findAll() {
        return (List) (em.createQuery("SELECT a FROM WorkSheet a").getResultList());
    }

    @Override
    public Optional<WorkSheet> findById(Integer id) {
        try {
            WorkSheet u = (WorkSheet) em.createQuery("SELECT w FROM WorkSheet w WHERE w.workSheetId=:id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(u);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public void deleteById(Integer id) {
        WorkSheet w = em.find(WorkSheet.class, id);
        if (w != null) {
            em.remove(w);
        }
    }

    @Override
    public void update(WorkSheet entity) {
        em.merge(entity);
    }

    @Override
    public void save(WorkSheet entity) {
        em.persist(entity); 
    }
    
    public List<WorkSheet> findByUserId(Integer id) {
        
        List w = (List) em.createQuery("SELECT w FROM WorkSheet w WHERE w.userId.userId = :id")
                .setParameter("id", id)
                .getResultList();
        
        if (!w.isEmpty()) {
            return w;
        }
        return new ArrayList<>();
    } 
}
