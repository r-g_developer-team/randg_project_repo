/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.fee;
import hu.braininghub.mobileserviceapplication.repository.CrudRepository;
import hu.braininghub.mobileserviceapplication.repository.entity.feeentity.Fee;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author momate
 */

@Singleton
@LocalBean
public class FeeDao implements CrudRepository<Fee, Integer>{
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Fee> findAll() {
        return (List)(em.createQuery("SELECT e FROM Fee e").getResultList());
    }
    @Override
    public Optional<Fee> findById(Integer id) {
        try {
            Fee f = (Fee) em.createQuery("SELECT f FROM Fee f WHERE f.feeId=:id")
                    .setParameter("id", id)
                    .getSingleResult();
            return Optional.of(f);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
    @Override
    public void deleteById(Integer id) {
        Fee f = em.find(Fee.class, id);
        if (f != null) {
            em.remove(f);
        }
    }
    @Override
    public void update(Fee entity) {
        em.merge(entity);
    }
    @Override
    public void save(Fee entity) {
        em.persist(entity);
    }
}
