/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.entity.feeentity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author george
 */
@Entity
@EqualsAndHashCode(of = {"feeId"})
@ToString
@Table (name = "fees")
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Fee implements Serializable{
    
    static final long serialVersionUID = 42L;

    public Fee(String feeName, String price) {
        this.feeName = feeName;
        this.price = price;
    }
    
    
    
    public Fee(){}
    
    @Id
    @Getter
    @Setter
    @Column (name = "fee_id")
    private Integer feeId;
    
    @Getter
    @Setter
    @Column (name = "fee_name")
    private String feeName;
    
    @Getter
    @Setter
    @Column (name = "price")
    private String price;
}
