/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity;

import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Leo
 */
@Entity
@EqualsAndHashCode(of = "workSheetId")
@ToString
@Table(name = "worksheet")
public class WorkSheet implements Serializable {

    static final long serialVersionUID = 1L;

    public WorkSheet() {
    }

    @Id
    @Getter
    @Setter
    @Column(name = "worksheet_id")
    private Integer workSheetId;

    @Getter
    @Setter
    @Column(name = "phone_type")
    private String phoneType;

    @Getter
    @Setter
    @Column(name = "imei_number")
    private Long imeiNumber;

    @Getter
    @Setter
    @Column(name = "issue_description")
    private String issueDescription;

    @Getter
    @Setter
    @Column(name = "ticket_creation_date")
    private String ticketCreationDate;

    @Getter
    @Setter
    @Column(name = "img_path")
    private String imgPath;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    @Column(name = "worksheet_status")
    private WorkSheetStatus worksheetStatus;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false, insertable = true, updatable = true)
    private User userId;
}
