/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.repository.entity;

/**
 *
 * @author Leo
 */
public enum WorkSheetStatus {
    WAITING_OFFER,
    UNTIL_REPAIRE, 
    REPAIRE_COMPLETED,
    DONE
}
