/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service.feeservice;

import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.mapper.FeeMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.feeentity.Fee;
import hu.braininghub.mobileserviceapplication.repository.fee.FeeDao;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Leo
 */
@Slf4j
@Stateless
public class FeeService {

    @Inject
    private FeeDao feeDao;

    @Inject
    private FeeMapper feeMapper;

    public List<FeeDto> listFeeDto() {

        List<FeeDto> result = new ArrayList();

        for (Fee e : feeDao.findAll()) {        
            result.add(feeMapper.toDto(e));
        }
        return result;
    }

    public void addFee(FeeDto fee) {

        log.info("New fee added: " + fee.toString());
        feeDao.save(feeMapper.toEntity(fee));
    }

    public void updateFee(FeeDto fee) {

        feeDao.update(feeMapper.toEntity(fee));
    }

    public void deleteFeeById(Integer feeId) {
        feeDao.deleteById(feeId);
    }

    public FeeDto filterFeeById(Integer feeId) {

        Optional<Fee> fees = feeDao.findById(feeId);

        return feeMapper.toDto(fees.get());
    }
    public void setFeeDao(FeeDao feeDao) {
        this.feeDao = feeDao;
    }
    
    public void setFeeMapper(FeeMapper feeMapper) {
        this.feeMapper=feeMapper;
    }
}
