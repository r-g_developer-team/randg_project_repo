/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service.userservice;

import hu.braininghub.mobileserviceapplication.dto.UserGroupDto.UserGroupDto;
import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.exceptions.EmailIsAlreadyExistException;
import hu.braininghub.mobileserviceapplication.mapper.UserMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import hu.braininghub.mobileserviceapplication.repository.entity.usergroupentity.UserGroup;
import hu.braininghub.mobileserviceapplication.repository.user.UserDao;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

@Slf4j
@Stateless
public class UserService {

    @Inject
    private UserDao userDao;

    @Inject
    @Setter
    private UserMapper userMapper;

    public UserDto findById(Integer userId) {

        Optional<User> users = userDao.findById(userId);

        return userMapper.toDto(users.get());
    }

    public List<UserDto> listUsers() {
        List<UserDto> result = new ArrayList();

        for (User e : userDao.findAll()) {
            result.add(userMapper.toDto(e));
        }
        return result;

    }

    public List<UserDto> listUsersWithRoles(List<UserDto> origin) {
        List<UserDto> result = new ArrayList();
        for (UserDto u : origin) {
            u.setAdmin(usersAndRoles(u));
            result.add(u);
        }
        return result;
    }

    private boolean usersAndRoles(UserDto u) {

        String email = u.getEmail();
        return userDao.isAdmin(email);

    }

    public void addUser(UserDto user) throws EmailIsAlreadyExistException {
        if (isEmailExist(user)) {
            throw new EmailIsAlreadyExistException();
        } else {
            userDao.save(userMapper.toEntity(user));
            log.debug("New user added " + user.toString());
        }

    }

    public void deleteUserById(Integer id) {
        
        UserDto dto = findById(id);
        userDao.deleteUserGroup(dto.getEmail());
        userDao.deleteById(id);
    }
    public void setEmailInUserGroup(String email, String origin){
        userDao.setEmailInUserGroup(email, origin);
    }

    public void update(UserDto user) {
        
        userDao.update(userMapper.toEntity(user));
    }

    public void addGroup(UserDto user) {
        UserGroupDto u = new UserGroupDto("user", user.getEmail());
        UserGroup uGroup = new UserGroup();
        try {
            BeanUtils.copyProperties(uGroup, u);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        userDao.save(uGroup);

    }

    public boolean isEmailExist(UserDto user) {

        String email = user.getEmail();

        return userDao.findByEmail(email).isPresent();

    }

    public String passwordSha256(String psw) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(psw.getBytes(Charset.forName("UTF-8")));

        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }

        return sb.toString();
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setRole(String email, String role) {

        userDao.setRole(email, role);
    }

    /**
     * This method returns with Optional<UserDto>. It uses the UserDao's method
     * findByEmail, which returns Optional<User>
     * If there is a user with the given e-mail parameter, You can get the
     * surched UserDto by the method userDto.get(). In other case This method
     * returns with Optional.empty()
     */
    public Optional<UserDto> findUserDtoByEmail(String email) {
        Optional<User> userEntity = userDao.findByEmail(email);
        if (userEntity.isPresent()) {
            return Optional.of(userMapper.toDto(userEntity.get()));
        }
        return Optional.empty();
    }

    /**
     * The parameter UserDto dto has already a new password! In this method the
     * dto will be converted to entity, and finally the entity will be updated
     * in database via UserDao - updatePassword method.
     */
    public void updatePassword(UserDto dto) {
        try {
            User user = userMapper.toEntity(dto);
            user.setPassword(passwordSha256(user.getPassword()));
            userDao.updatePassword(user);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
