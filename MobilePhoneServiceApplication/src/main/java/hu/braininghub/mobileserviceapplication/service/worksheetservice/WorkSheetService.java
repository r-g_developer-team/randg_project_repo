/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service.worksheetservice;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.mapper.WorkSheetMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import hu.braininghub.mobileserviceapplication.repository.user.UserDao;
import hu.braininghub.mobileserviceapplication.repository.worksheet.WorkSheetDao;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Leo
 */
@Stateless
public class WorkSheetService {

    @Inject
    private WorkSheetDao workSheetDao;

    @Inject
    private WorkSheetMapper workSheetMapper;

    @Inject
    private UserDao userDao;
    
    public List<WorkSheetDto> listWorkSheets(){
        List<WorkSheetDto> worksheetDto = new ArrayList();
        
        workSheetDao.findAll().forEach((workSheet) -> {
            worksheetDto.add(workSheetMapper.toDto(workSheet));
        });
        
        return worksheetDto;
    }
    
    public WorkSheetDto findWorksheetById(Integer worksheetId){
    
        Optional<WorkSheet> entity = workSheetDao.findById(worksheetId);
        return workSheetMapper.toDto(entity.get());
    }

    public List<WorkSheetDto> listWorksheetsByMail(String mail) {
        List<WorkSheetDto> dto = new ArrayList();
        
        Optional<User> userByMail = userDao.findByEmail(mail);
        workSheetDao.findByUserId(userByMail.get().getUserId()).forEach((workSheet) -> {
            dto.add(workSheetMapper.toDto(workSheet));
        });
        
        return dto;
    }

    public void addWorkSheet(WorkSheetDto dto, String mail) {

        WorkSheet result = workSheetMapper.toEntity(dto);
        
        Optional<User> userbyId = userDao.findByEmail(mail);

        result.setUserId(userbyId.get());

        workSheetDao.save(result);
    }
    
    public void updateWorkSheet(WorkSheetDto dto){
        
        WorkSheet entity = workSheetMapper.toEntity(dto);
        
        User userByWorksheetId = userDao.findUserByWorksheetId(entity).get();
        
        entity.setUserId(userByWorksheetId);
        
        workSheetDao.update(entity);
    }
    
    public void deleteWorksheetById(Integer id){         
        workSheetDao.deleteById(id);
    }

    public void setWorkSheetDao(WorkSheetDao workSheetDao) {
        this.workSheetDao = workSheetDao;
    }
    
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setWorkSheetMapper(WorkSheetMapper workSheetMapper) {
        this.workSheetMapper = workSheetMapper;
    }
}
