/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.forgotPasswordCases;

import java.util.regex.Pattern;

/**
 *
 * @author levsz
 */
public class EmailFormChecker {

    /**
     * You give an e-mail input as String into this method's parameter. It will
     * return true, if the e-mail form is correct
     */
    public static boolean hasEmailCorrectForm(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

}
