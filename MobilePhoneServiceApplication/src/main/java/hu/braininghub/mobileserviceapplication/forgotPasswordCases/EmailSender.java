/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.forgotPasswordCases;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Session;
import javax.mail.Transport;

/**
 *
 * @author levsz
 */
public class EmailSender {

    private static final String SENDER = "robertgidak2020@gmail.com";
    private static final String EMAIL_PW = "robigida2020";

    static class Auth extends Authenticator {

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(SENDER, EMAIL_PW);
        }
    }

    private Session getSessionWithAuthenticationAndProperties() {
        Properties propNew = new Properties();
        propNew.put("mail.transport.protocol", "smtp");
        propNew.put("mail.smtp.auth", "true");
        propNew.put("mail.smtp.starttls.enable", "true");
        propNew.put("mail.smtp.host", "smtp.gmail.com");
        propNew.put("mail.smtp.port", "587");
        propNew.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        propNew.put("mail.debug", "true");
        return Session.getInstance(propNew, new Auth());
    }

    public void sendMessageWithNewPassword(String recipient, String password) {
        Session session = getSessionWithAuthenticationAndProperties();
        Message message = prepareMessageWithNewPassword(session, SENDER, recipient, password);
        try {
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendMessageFromContactPage(String userName, String userEmailAddress, String subject, String text) {
        Session session = getSessionWithAuthenticationAndProperties();
        Message message = prepareMessageFromContactPage(session, userName, userEmailAddress, subject, text);
        try {
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This is a general e-mail sender method. The constructor includes the
     * recipient's address and name, the sender address, the subject and the
     * text body, which start as "Tisztelt {user name}! \n \n {text}...
     */
    public void sendGeneralEmailToUser(String userName, String userEmailAddress, String subject, String text) {
        Session session = getSessionWithAuthenticationAndProperties();
        Message message = prepareGeneralEmailToUser(session, userName, userEmailAddress, subject, text);
        try {
            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Message prepareMessageWithNewPassword(Session session, String sender, String recipient, String pw) {
        String subject = "Új jelszó MobilePhoneService alkalmazáshoz";
        try {
            InternetAddress recipientAddress = new InternetAddress(recipient);
            InternetAddress senderAddress = new InternetAddress(sender);
            Message msg = new MimeMessage(session);
            msg.setFrom(senderAddress);
            msg.setRecipient(Message.RecipientType.TO, recipientAddress);
            msg.setSubject(subject);
            msg.setText("Tisztelt Regisztrálónk! \n \n "
                    + "Új jelszót kért profiljához. Az új jelszava: " + pw + ". Kérjük lépjen be, majd módosítsa jelszavát! \n \n"
                    + "Üdvözlettel: \n \n Robert&Gidak (dream)Team");
            return msg;
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Message prepareMessageFromContactPage(Session session, String userName, String userAddress, String subject, String text) {
        try {
            InternetAddress senderAddress = new InternetAddress(SENDER);
            Message msg = new MimeMessage(session);
            msg.setFrom(senderAddress);
            msg.setRecipient(Message.RecipientType.TO, senderAddress);
            msg.setSubject(subject);
            msg.setText("Feladó: " + userAddress + "\n"
                    + "Név: " + userName + " \n \n"
                    + text);
            return msg;
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Message prepareGeneralEmailToUser(Session session, String userName, String recipient, String subject, String text) {
        try {
            InternetAddress senderAddress = new InternetAddress(SENDER);
            InternetAddress recipientAddress = new InternetAddress(recipient);
            Message msg = new MimeMessage(session);
            msg.setFrom(senderAddress);
            msg.setRecipient(Message.RecipientType.TO, recipientAddress);
            msg.setSubject(subject);
            msg.setText("Tisztelt " + userName + "! \n \n" + text);
            return msg;
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
