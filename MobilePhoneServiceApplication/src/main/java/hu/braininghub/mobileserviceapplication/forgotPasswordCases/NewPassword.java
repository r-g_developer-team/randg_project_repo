/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.forgotPasswordCases;


/**
 *
 * @author levsz
 */
public class NewPassword {

    public static final int NUMBER_FROM = 100;
    public static final int NUMBER_TO = 1000;
    public static final char CHARACTER_FROM = 'a';
    public static final char CHARACTER_TO = 'z';

    public NewPassword() {
    }

    /**
     * It makes 6 digits random password. Three character and three number. This
     * method invokes two other methods to implement this task:
     * generateCharacter() and generateNumberBetween100and999()
     */
    public static String generateNewPassword() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 3; i++) {
            sb.append(generateCharacter());
        }
        sb.append(generateNumberBetween100and999());
        String newPassword = sb.toString();
        //sendNewPasswordViaEmail(newPassword);
        return newPassword;
    }

    private static char generateCharacter() {
        int from = (int) CHARACTER_FROM;
        int to = (int) CHARACTER_TO;
        double resultDouble = (double) Math.random() * (to - from) + from;
        int resultInt = (int) resultDouble;
        return (char) resultInt;
    }

    private static int generateNumberBetween100and999() {
        double resultDouble = (double) Math.random() * (NUMBER_TO - NUMBER_FROM) + NUMBER_FROM;
        int resultInt = (int) resultDouble;
        return resultInt;
    }
}
