/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 *
 * @author s26841
 */
@Singleton
@LocalBean
public class WorkSheetMapper implements Mapper<WorkSheet, WorkSheetDto>{
    
    @Override
    public WorkSheet toEntity(WorkSheetDto dto) {
        WorkSheet entity = new WorkSheet();
        entity.setWorkSheetId(dto.getWorkSheetId());
        entity.setPhoneType(dto.getPhoneType());
        entity.setImeiNumber(dto.getImeiNumber());
        entity.setIssueDescription(dto.getIssueDescription());
        entity.setTicketCreationDate(dto.getTicketCreationDate());
        entity.setImgPath(dto.getImgPath());
        entity.setWorksheetStatus(dto.getWorksheetStatus());
        
        return entity;
    }

    @Override
    public WorkSheetDto toDto(WorkSheet entity) {
        WorkSheetDto dto = new WorkSheetDto();
        dto.setWorkSheetId(entity.getWorkSheetId());
        dto.setPhoneType(entity.getPhoneType());
        dto.setImeiNumber(entity.getImeiNumber());
        dto.setIssueDescription(entity.getIssueDescription());
        dto.setTicketCreationDate(entity.getTicketCreationDate());
        dto.setImgPath(entity.getImgPath());
        dto.setWorksheetStatus(entity.getWorksheetStatus());
        
        return dto;
    }
}
