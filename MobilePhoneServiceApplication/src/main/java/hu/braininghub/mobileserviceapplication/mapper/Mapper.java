/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

/**
 *
 * @author s26841
 */
public interface Mapper<Entity, Dto> {
    
    Entity toEntity(Dto dto);
    
    Dto toDto(Entity entity);
}
