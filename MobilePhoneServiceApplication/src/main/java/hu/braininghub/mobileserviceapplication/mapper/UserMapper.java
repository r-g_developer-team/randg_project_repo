/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author s26841
 */
@Singleton
@LocalBean
public class UserMapper implements Mapper<User, UserDto>{

    @Inject
    private UserService service;
    
    @Override
    public User toEntity(UserDto dto) {
        User entity = new User();
        entity.setUserId(dto.getUserId());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPostalCode(dto.getPostalCode());
        entity.setPassword(dto.getPassword());
        entity.setCity(dto.getCity());
        entity.setEmail(dto.getEmail());
        
        return entity;
    }

    @Override
    public UserDto toDto(User entity) {
        UserDto dto = new UserDto();
        dto.setUserId(entity.getUserId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPostalCode(entity.getPostalCode());
        dto.setPassword(entity.getPassword());
        dto.setCity(entity.getCity());
        dto.setEmail(entity.getEmail());
        
        
        return dto;
    }
    
}
