/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.repository.entity.feeentity.Fee;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 *
 * @author Leo
 */
@Singleton
@LocalBean
public class FeeMapper implements Mapper<Fee, FeeDto>{

    @Override
    public Fee toEntity(FeeDto dto) {
        Fee entity=new Fee();
        entity.setFeeId(dto.getFeeId());
        entity.setFeeName(dto.getFeeName());
        entity.setPrice(dto.getPrice());
        
        return entity;
    }

    @Override
    public FeeDto toDto(Fee entity) {
        FeeDto dto = new FeeDto();
        dto.setFeeId(entity.getFeeId());
        dto.setFeeName(entity.getFeeName());
        dto.setPrice(entity.getPrice());
        
        return dto;
                
    }
    
}
