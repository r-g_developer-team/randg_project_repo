/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.exceptions;

/**
 *
 * @author momate
 */
public class EmailIsAlreadyExistException extends Exception {

    public String msg() {
        return "Az e-mail-cím már létezik";
    }

}
