/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.adminservlet;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author george
 */
@WebServlet(name = "AdminWorksheetServlet", urlPatterns = {"/AdminWorksheetServlet"})
public class AdminWorksheetServlet extends HttpServlet implements Serializable{

    static final long serialVersionUID = 82L;
    
    @Inject
    private transient WorkSheetService workSheetService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<WorkSheetDto> worksheetList = workSheetService.listWorkSheets();
        request.setAttribute("worksheets", worksheetList);
        request.getRequestDispatcher("WEB-INF/admin/worksheets.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer worksheetId = Integer.parseInt(request.getParameter("worksheetId"));
        
        WorkSheetDto worksheetById = workSheetService.findWorksheetById(worksheetId);
        
        request.setAttribute("worksheet", worksheetById);
        request.getRequestDispatcher("/WEB-INF/admin/adminworksheeteditor.jsp").forward(request, response);
    }
}
