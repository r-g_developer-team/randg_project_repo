/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.feeservlet;

import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;

import hu.braininghub.mobileserviceapplication.service.feeservice.FeeService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author levsz
 */
@WebServlet(name = "FeeServlet", urlPatterns = {"/FeeServlet"})
public class FeeServlet extends HttpServlet {

    static final long serialVersionUID = 42L;

    @Inject
    private transient FeeService feeService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<FeeDto> fees = feeService.listFeeDto();

        request.setAttribute("fees", fees);
        request.getRequestDispatcher("fee.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String feeName = request.getParameter("feeName");
        String price = request.getParameter("price");

        FeeDto dto = new FeeDto(Integer.valueOf(id), feeName, price);

        if (request.getParameter("create") != null) {

            feeService.addFee(dto);

        } else if (request.getParameter("update") != null) {

            feeService.updateFee(dto);

        } else if (request.getParameter("delete") != null) {

            feeService.deleteFeeById(Integer.valueOf(id));

        }

        request.getRequestDispatcher("fee.jsp").forward(request, response);
    }
}
