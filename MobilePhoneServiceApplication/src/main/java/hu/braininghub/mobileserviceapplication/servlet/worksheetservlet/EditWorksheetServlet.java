/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.worksheetservlet;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author george
 */
@WebServlet(name = "EditWorksheetServlet", urlPatterns = {"/EditWorksheetServlet"})
public class EditWorksheetServlet extends HttpServlet implements Serializable{
    
    static final long serialVersionUID = 9L;
    
    @Inject
    private transient WorkSheetService worksheetService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Integer worksheetId = Integer.parseInt(request.getParameter("worksheetId"));
        
        WorkSheetDto worksheetById = worksheetService.findWorksheetById(worksheetId);
        
        request.setAttribute("worksheet", worksheetById);
        request.getRequestDispatcher("/WEB-INF/worksheet-editor.jsp").forward(request, response);
    }
}
