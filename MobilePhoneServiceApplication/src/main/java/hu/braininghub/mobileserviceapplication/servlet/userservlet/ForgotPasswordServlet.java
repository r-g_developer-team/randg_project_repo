/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.forgotPasswordCases.EmailFormChecker;
import hu.braininghub.mobileserviceapplication.forgotPasswordCases.EmailSender;
import hu.braininghub.mobileserviceapplication.forgotPasswordCases.NewPassword;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author levsz
 */
@WebServlet(name = "ForgotPasswordServlet", urlPatterns = {"/ForgotPasswordServlet"})
public class ForgotPasswordServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient UserService userService;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String emailInput = request.getParameter("email");
        System.out.println("ForgotPasswordServlet-doGet-'email' parameter from request is: " + emailInput);
        Optional<UserDto> optionalDto = userService.findUserDtoByEmail(emailInput);
        System.out.println("ForgotPasswordServlet-doGet-optionalDto: " + optionalDto);

        if (optionalDto.isPresent()) {
            UserDto userDto = optionalDto.get();
            //set new password for dto
            String password = NewPassword.generateNewPassword();
            userDto.setPassword(password);
            //update this dto (via mapper) to db
            userService.updatePassword(userDto);
            //send e-mail to user
            EmailSender emailSender = new EmailSender();
            emailSender.sendMessageWithNewPassword(emailInput, password);
            request.setAttribute("message", "A megadott " + emailInput + " címre küldjük új jelszavát.");
            request.getRequestDispatcher("/WEB-INF/sendingNewPassword.jsp").forward(request, response);
        } else if (!EmailFormChecker.hasEmailCorrectForm(emailInput)) {
            request.setAttribute("message", "A megadott " + emailInput + " cím nem helyes formátumú.");
            request.getRequestDispatcher("/WEB-INF/incorrectEmail.jsp").forward(request, response);
        } else if (!optionalDto.isPresent()) {
            request.setAttribute("message", "A megadott " + emailInput + " címmel nem létezik regisztráció honlapunkon.");
            request.getRequestDispatcher("/WEB-INF/incorrectEmail.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
