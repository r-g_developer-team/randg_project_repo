/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author momate
 */
@Slf4j
@WebServlet(name = "AdminUserEditorServlet", urlPatterns = {"/AdminUserEditorServlet"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin"}))
public class AdminUserEditorServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient UserService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<UserDto> usersListWithRoles = service.listUsersWithRoles(service.listUsers());
        request.setAttribute("usersList", usersListWithRoles);
        request.getRequestDispatcher("WEB-INF/admin/users.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("userId"));

        if (request.getParameter("update") != null) {
            String firstName = request.getParameter("userFName");
            String lastName = request.getParameter("userLName");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phone");
            String postalCode = request.getParameter("postal");
            String city = request.getParameter("city");
            String password = request.getParameter("pw");

            UserDto dto = new UserDto(id, firstName, lastName, email, phoneNumber, postalCode, city, password);

            service.setEmailInUserGroup(email, request.getParameter("origin"));
            service.update(dto);
            service.setRole(email, request.getParameter("roles"));

        } else if (request.getParameter("delete") != null) {
            service.deleteUserById(id);
            
        }

        doGet(request, response);
    }

}
