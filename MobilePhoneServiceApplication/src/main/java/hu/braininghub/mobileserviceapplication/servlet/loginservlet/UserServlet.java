/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.loginservlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author momate
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/UserServlet"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "user"}))
public class UserServlet extends HttpServlet {

    static final long serialVersionUID = 42L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        

    }
}
