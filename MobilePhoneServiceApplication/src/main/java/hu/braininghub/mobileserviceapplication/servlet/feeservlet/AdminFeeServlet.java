/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.feeservlet;

import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.service.feeservice.FeeService;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author momate
 */
@Slf4j
@WebServlet(name = "AdminFeeServlet", urlPatterns = {"/AdminFeeServlet"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin"}))
public class AdminFeeServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient FeeService feeService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<FeeDto> fees = feeService.listFeeDto();
        System.out.println(fees.toString());
        log.info(fees.toString());
        log.debug(fees.toString());

        request.setAttribute("fees", fees);
        request.getRequestDispatcher("WEB-INF/admin/fees.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer feeId = Integer.parseInt(request.getParameter("feeId"));

        if (feeId != 0) {
            
            FeeDto feeById = feeService.filterFeeById(feeId);
            request.setAttribute("fee", feeById);
        }

        request.getRequestDispatcher("/WEB-INF/admin/feesEditor.jsp").forward(request, response);
    }
}
