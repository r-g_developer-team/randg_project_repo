/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.forgotPasswordCases.EmailSender;
import java.io.IOException;
import java.io.Serializable;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author levsz
 */
@WebServlet(name = "UserEmailServlet", urlPatterns = {"/UserEmailServlet"})
public class UserEmailServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String emailAddress = request.getParameter("email");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
        String userName = request.getParameter("name");
        EmailSender email = new EmailSender();
        email.sendMessageFromContactPage(userName, emailAddress, subject, message);
        request.setAttribute("email", emailAddress);
        request.setAttribute("userName", userName);
        request.getRequestDispatcher("contact.jsp").forward(request, response);
    }

}
