/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"user"}))
@WebServlet(name = "UserServlet", urlPatterns = {"/UserServlet"})
public class UserServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 99L;

    @Inject
    private transient UserService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mail = request.getRemoteUser();
        UserDto userDto = service.findUserDtoByEmail(mail).get();
        String firstName = userDto.getFirstName();
        String lastName = userDto.getLastName();
        String city = userDto.getCity();
        String email = userDto.getEmail();
        String phoneNumber = userDto.getPhoneNumber();
        String postalCode = userDto.getPostalCode();
        String password = userDto.getPassword();
        request.setAttribute("firstname", firstName);
        request.setAttribute("lastname", lastName);
        request.setAttribute("city", city);
        request.setAttribute("email", email);
        request.setAttribute("phonenumber", phoneNumber);
        request.setAttribute("postalcode", postalCode);

        String newFirstName = request.getParameter("firstName");
        if (newFirstName != null) {
            userDto.setFirstName(newFirstName);
            userDto.setLastName(lastName);
            userDto.setEmail(email);
            userDto.setPhoneNumber(phoneNumber);
            userDto.setPostalCode(postalCode);
            userDto.setCity(city);
            userDto.setPassword(password);
            service.update(userDto);
        }
        String newLastName = request.getParameter("lastName");
        if (newLastName != null) {
            userDto.setFirstName(firstName);
            userDto.setLastName(newLastName);
            userDto.setEmail(email);
            userDto.setPhoneNumber(phoneNumber);
            userDto.setPostalCode(postalCode);
            userDto.setCity(city);
            userDto.setPassword(password);
            service.update(userDto);
        }
        String newEmail = request.getParameter("email");
        if (newEmail != null) {
            userDto.setFirstName(firstName);
            userDto.setLastName(lastName);
            userDto.setEmail(newEmail);
            userDto.setPhoneNumber(phoneNumber);
            userDto.setPostalCode(postalCode);
            userDto.setCity(city);
            userDto.setPassword(password);
            service.update(userDto);
        }
        String newPhoneNumber = request.getParameter("phoneNumber");
        if (newPhoneNumber != null) {
            userDto.setFirstName(firstName);
            userDto.setLastName(lastName);
            userDto.setEmail(email);
            userDto.setPhoneNumber(newPhoneNumber);
            userDto.setPostalCode(postalCode);
            userDto.setCity(city);
            userDto.setPassword(password);
            service.update(userDto);
        }
        String newPostalCode = request.getParameter("postalCode");
        if (newPostalCode != null) {
            userDto.setFirstName(firstName);
            userDto.setLastName(lastName);
            userDto.setEmail(email);
            userDto.setPhoneNumber(phoneNumber);
            userDto.setPostalCode(newPostalCode);
            userDto.setCity(city);
            userDto.setPassword(password);
            service.update(userDto);
        }

        request.getRequestDispatcher("usereditor.jsp").forward(request, response);
    }

}
