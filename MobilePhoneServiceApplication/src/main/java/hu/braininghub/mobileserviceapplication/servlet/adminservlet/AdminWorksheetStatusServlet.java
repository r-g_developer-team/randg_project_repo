/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.adminservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author george
 */
@WebServlet(name = "AdminWorksheetStatusServlet", urlPatterns = {"/AdminWorksheetStatusServlet"})
public class AdminWorksheetStatusServlet extends HttpServlet implements Serializable{

    static final long serialVersionUID = 98L;
    
    @Inject
    private transient WorkSheetService worksheetService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<WorkSheetDto> worksheetList = worksheetService.listWorkSheets();
        request.setAttribute("worksheets", worksheetList);
        request.getRequestDispatcher("WEB-INF/admin/worksheets.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        WorkSheetStatus workSheetStatus;
        
        Integer worksheetId = Integer.parseInt(request.getParameter("worksheetId"));
        String phoneType = request.getParameter("phoneType");
        Long imeiNumber = Long.parseLong(request.getParameter("imeiNumber"));
        String issueDescription = request.getParameter("issueDescription");
        String imgPath = request.getParameter("imgPath");
        String ticketCreationDate = request.getParameter("ticketCreationDate");
        
        if ("WAITING_OFFER".equalsIgnoreCase(request.getParameter("worksheetStatus"))) {
            
            workSheetStatus = WorkSheetStatus.WAITING_OFFER;
            
        }else if ("UNTIL_REPAIRE".equalsIgnoreCase(request.getParameter("worksheetStatus"))){
            
            workSheetStatus = WorkSheetStatus.UNTIL_REPAIRE;
            
        }else if ("REPAIRE_COMPLETED".equalsIgnoreCase(request.getParameter("worksheetStatus"))){
            
            workSheetStatus = WorkSheetStatus.REPAIRE_COMPLETED;
            
        }else{

            workSheetStatus = WorkSheetStatus.DONE;

        }
        

        WorkSheetDto dto = new WorkSheetDto(worksheetId,
                phoneType,
                imeiNumber,
                issueDescription,
                ticketCreationDate,
                imgPath,
                workSheetStatus,
                new UserDto());

        if (request.getParameter("update") != null) {

            worksheetService.updateWorkSheet(dto);

        } else if (request.getParameter("delete") != null) {

            worksheetService.deleteWorksheetById(worksheetId);
        }
            
        doGet(request, response); 
    }
}
