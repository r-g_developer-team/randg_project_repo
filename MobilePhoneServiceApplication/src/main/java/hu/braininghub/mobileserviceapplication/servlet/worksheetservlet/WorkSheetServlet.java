/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.worksheetservlet;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author momate
 */
@Slf4j
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "user"}))
@WebServlet(name = "WorkSheetServlet", urlPatterns = {"/WorkSheetServlet"})
public class WorkSheetServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 1L;

    @Inject
    private WorkSheetDto dto;

    @Inject
    private transient WorkSheetService workSheetService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String mail = request.getRemoteUser();
        List<WorkSheetDto> worksheetList = workSheetService.listWorksheetsByMail(mail);
        request.setAttribute("worksheets", worksheetList);
        request.getRequestDispatcher("worksheet.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            if (saveInputDatas(request)) {
                String mail = request.getRemoteUser();
                workSheetService.addWorkSheet(dto, mail);
            }
        } catch (Exception ex) {
            Logger.getLogger(WorkSheetServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        doGet(request, response);
    }

    private boolean saveInputDatas(HttpServletRequest request) throws Exception {

        boolean uploadStatus = false;
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            String serverPath = "/Users/george/JAVA_apps/payara5/glassfish/domains/domain1/applications/mobileservic/images/upload-images";
            String realpath = "images/upload-images";
            try {
                List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : items) {
                    if (!item.isFormField()) {
                        LocalDateTime fileUploadDate = LocalDateTime.now();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssms");
                        String formatDateTime = fileUploadDate.format(formatter);
                        String filePathForServer = serverPath + File.separator + "image" +formatDateTime + item.getName();
                        String filePathForDatabase = realpath + File.separator + "image" + formatDateTime + item.getName();
                        File newFile = new File(filePathForServer);
                        item.write(newFile);
                        dto.setImgPath(filePathForDatabase);
                    } else {
                        dto.setTicketCreationDate(LocalDateTime.now().toString());
                        dto.setWorksheetStatus(WorkSheetStatus.WAITING_OFFER);

                        if (item.getFieldName().equals("phoneType")) {
                            dto.setPhoneType(item.getString());
                        }
                        if (item.getFieldName().equals("imeiNumber")) {
                            dto.setImeiNumber(Long.parseLong(item.getString()));
                        }
                        if (item.getFieldName().equals("issueDescription")) {
                            dto.setIssueDescription(item.getString());
                        }
                    }
                }
                request.setAttribute("message", "Sikeres képfeltöltés.");
                uploadStatus = true;
                log.info("images upload successful!");
                return uploadStatus;

            } catch (FileNotFoundException | FileUploadException ex) {
                request.setAttribute("message", "A képet nem sikerült feltölteni.");
                Logger.getLogger(WorkSheetServlet.class.getName()).log(Level.SEVERE, null, ex);
                return uploadStatus;
            }
        } else {
            request.setAttribute("message", "Nem megfelelő fájlt töltöttél fel.");
            return uploadStatus;
        }
    }
}
