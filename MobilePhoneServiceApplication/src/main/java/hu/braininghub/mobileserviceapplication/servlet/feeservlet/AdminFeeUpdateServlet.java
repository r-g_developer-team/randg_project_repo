/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.feeservlet;

import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.service.feeservice.FeeService;
import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author momate
 */
@Slf4j
@WebServlet(name = "AdminFeeUpdateServlet", urlPatterns = {"/AdminFeeUpdateServlet"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin"}))
public class AdminFeeUpdateServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient FeeService feeService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("fees", feeService.listFeeDto());
        request.getRequestDispatcher("WEB-INF/admin/fees.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String feeName = request.getParameter("feeName");
        String feePrice = request.getParameter("feePrice");

        if (request.getParameter("add") != null) {
            FeeDto dto = new FeeDto(feeName, feePrice);
            feeService.addFee(dto);

        } else if (request.getParameter("update") != null) {
            Integer feeId = Integer.parseInt(request.getParameter("feeId"));
            FeeDto dto = new FeeDto(feeId, feeName, feePrice);
            feeService.updateFee(dto);

        } else if (request.getParameter("delete") != null) {
            Integer feeId = Integer.parseInt(request.getParameter("feeId"));
            feeService.deleteFeeById(feeId);
        }

        doGet(request, response);
    }
}
