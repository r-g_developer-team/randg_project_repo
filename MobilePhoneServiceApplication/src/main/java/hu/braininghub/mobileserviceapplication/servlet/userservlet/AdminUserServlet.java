/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import java.io.IOException;

import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author momate
 */
@Slf4j
@WebServlet(name = "AdminUserServlet", urlPatterns = {"/users"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin"}))
public class AdminUserServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient UserService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<UserDto> usersListWithRoles = service.listUsersWithRoles(service.listUsers());

        log.info("Users: " + usersListWithRoles.toString());

        request.setAttribute("usersList", usersListWithRoles);
        request.getRequestDispatcher("WEB-INF/admin/users.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("userId"));

        UserDto dto = service.findById(id);

        request.setAttribute("user", dto);
        request.getRequestDispatcher("/WEB-INF/admin/userEditor.jsp").forward(request, response);
    }

}
