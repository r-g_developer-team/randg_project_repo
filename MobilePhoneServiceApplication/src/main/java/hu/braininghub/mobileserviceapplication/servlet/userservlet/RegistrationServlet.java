/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.userservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.exceptions.EmailIsAlreadyExistException;
import hu.braininghub.mobileserviceapplication.forgotPasswordCases.EmailSender;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author momate
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = {"/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 42L;

    @Inject
    private transient UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");
        String postalCode = request.getParameter("postalCode");
        String city = request.getParameter("city");
        String password = "";
        try {
            password = userService.passwordSha256(request.getParameter("psw"));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        UserDto user = new UserDto(
                firstName,
                lastName,
                email,
                phoneNumber,
                postalCode,
                city,
                password
        );
        try {
            userService.addUser(user);
            userService.addGroup(user);
            EmailSender emailSender = new EmailSender();
            emailSender.sendGeneralEmailToUser(lastName + " " + firstName, email,
                    "Regisztráció", "Regisztrációja sikeresen megtörtént! Köszönjük bizalmát! \n \n"
                    + "Üdvözlettel: \n \n Robert&Gidak (dream)Team");
        } catch (EmailIsAlreadyExistException ex) {
            request.setAttribute("errMessage", ex.msg());

        }

        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

}
