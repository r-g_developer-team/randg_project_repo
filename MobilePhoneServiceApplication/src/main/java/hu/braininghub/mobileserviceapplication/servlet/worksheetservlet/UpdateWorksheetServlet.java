/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.servlet.worksheetservlet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author george
 */
@Slf4j
@WebServlet(name = "UpdateWorksheetServlet", urlPatterns = {"/UpdateWorksheetServlet"})
public class UpdateWorksheetServlet extends HttpServlet implements Serializable {

    static final long serialVersionUID = 8L;

    @Inject
    private transient WorkSheetService worksheetService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String mail = request.getRemoteUser();
        List<WorkSheetDto> worksheetList = worksheetService.listWorksheetsByMail(mail);
        request.setAttribute("worksheets", worksheetList);
        request.getRequestDispatcher("worksheet.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer worksheetId = Integer.parseInt(request.getParameter("worksheetId"));
        String phoneType = request.getParameter("phoneType");
        Long imeiNumber = Long.parseLong(request.getParameter("imeiNumber"));
        String issueDescription = request.getParameter("issueDescription");
        String imgPath = request.getParameter("imgPath");
        String ticketCreationDate = request.getParameter("ticketCreationDate");
        WorkSheetStatus workSheetStatus = WorkSheetStatus.valueOf(request.getParameter("worksheetStatus"));

        WorkSheetDto dto = new WorkSheetDto(worksheetId,
                phoneType,
                imeiNumber,
                issueDescription,
                ticketCreationDate,
                imgPath,
                workSheetStatus,
                new UserDto());

        if (request.getParameter("update") != null) {

            worksheetService.updateWorkSheet(dto);

        } else if (request.getParameter("delete") != null) {

            worksheetService.deleteWorksheetById(worksheetId);
        }
            
        doGet(request, response);
    }
}
