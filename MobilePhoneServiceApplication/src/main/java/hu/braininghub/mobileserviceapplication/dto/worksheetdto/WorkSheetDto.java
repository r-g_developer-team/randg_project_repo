/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.dto.worksheetdto;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import java.io.Serializable;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author momate
 */

@Singleton
@EqualsAndHashCode(of = {"workSheetId"})
@ToString
@LocalBean
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class WorkSheetDto implements Serializable {

    static final long serialVersionUID = 4L;

    public WorkSheetDto(String phoneType, Long imeiNumber, String issueDescription, 
                        String ticketCreationDate, String imgPath, 
                        WorkSheetStatus worksheetStatus) {
        
        this.phoneType = phoneType;
        this.imeiNumber = imeiNumber;
        this.issueDescription = issueDescription;
        this.ticketCreationDate = ticketCreationDate;
        this.imgPath = imgPath;
        this.worksheetStatus = worksheetStatus;
    }

    public WorkSheetDto() {
    }

    @Getter
    @Setter
    private Integer workSheetId;

    @Getter
    @Setter
    private String phoneType;

    @Getter
    @Setter
    private Long imeiNumber;

    @Getter
    @Setter
    private String issueDescription;

    @Getter
    @Setter
    private String ticketCreationDate;

    @Getter
    @Setter
    private String imgPath;

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    private WorkSheetStatus worksheetStatus;

    @Getter
    @Setter
    private UserDto user;
}
