/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.dto.UserGroupDto;

import java.io.Serializable;
import javax.ejb.Singleton;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;


/**
 *
 * @author momate
 */
@Singleton
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Data
public class UserGroupDto implements Serializable {

    static final long serialVersionUID = 2L;

    private String group;

    private String email;

    public UserGroupDto() {
    }



}
