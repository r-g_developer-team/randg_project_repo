/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.dto.userdto;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import java.io.Serializable;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author george
 */
@LocalBean
@ToString
@EqualsAndHashCode(of = {"userId"})
@Stateless
public class UserDto implements Serializable {

    static final long serialVersionUID = 3L;

    public UserDto() {
    }

    public UserDto(Integer userId) {
        this.userId = userId;
    }

    public UserDto(String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
        
    }

    public UserDto(Integer userId, String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
       
    }

    public UserDto( String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password, boolean isAdmin) {
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public UserDto(Integer userId, String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password, boolean isAdmin) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    @Getter
    @Setter
    private Integer userId;

    @Getter
    @Setter
    private List<WorkSheetDto> workSheets;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String phoneNumber;

    @Getter
    @Setter
    private String postalCode;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private boolean isAdmin;
}
