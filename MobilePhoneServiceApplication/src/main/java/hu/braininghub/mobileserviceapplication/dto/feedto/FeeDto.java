/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.dto.feedto;

import java.io.Serializable;
import javax.ejb.Singleton;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author george
 */
@Singleton
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class FeeDto implements Serializable {

    static final long serialVersionUID = 42L;

    public FeeDto(String feeName, String price) {
        this.feeName = feeName;
        this.price = price;
    }

    @Getter
    @Setter
    private Integer feeId;

    @Getter
    @Setter
    private String feeName;

    @Getter
    @Setter
    private String price;

    public FeeDto() {
    }
}
