
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <h5 class="card-header info-color white-text text-center py-4">
                            <strong>Felhasználó adatmódosítás</strong>
                        </h5>                    
                        <div class="card-body px-lg-5 pt-0">
                            <form class="" style="color: #757575;" method="POST" action="UserServlet"> 
                                <div class="input-group mb-3">
                                    <input id="firstName" name="firstName" type="text" class="form-control" placeholder="Vezetéknév">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Módosítás</button>
                                    </div>  
                                </div>
                            </form>
                            <form class="" style="color: #757575;" method="POST" action="UserServlet"> 
                                <div class="input-group mb-3">
                                    <input id="lastName" name="lastName" type="text" class="form-control" placeholder="Keresztnév">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Módosítás</button>
                                    </div>  
                                </div>
                            </form>
                            <form class="" style="color: #757575;" method="POST" action="UserServlet"> 
                                <div class="input-group mb-3">
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Email">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Módosítás</button>
                                    </div>  
                                </div>
                            </form>
                            <form class="" style="color: #757575;" method="POST" action="UserServlet"> 
                                <div class="input-group mb-3">
                                    <input id="phoneNumber" name="phoneNumber" type="text" class="form-control" placeholder="Telefonszám">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Módosítás</button>
                                    </div>  
                                </div>
                            </form>
                            <form class="" style="color: #757575;" method="POST" action="UserServlet"> 
                                <div class="input-group mb-3">
                                    <input id="postalCode" name="postalCode" type="text" class="form-control" placeholder="Irányítószám">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit">Módosítás</button>
                                    </div>  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <h5 class="card-header info-color white-text text-center py-4">
                            <strong>Felhasználó adatai</strong>
                        </h5>
                    </div>    
                    <div class="accordion" id="accordion"> 
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-bottom-0 border-top-0 border-left-0">
                                <p>
                                    <strong>Vezetéknév: </strong>
                                <div>${firstname} </div><br>
                            </div> 
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <p>
                                    <strong>Keresztnév: </strong>
                                <div>${lastname} </div><br>
                            </div> 
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <p>
                                    <strong>Város: </strong>
                                <div>${city} </div><br>
                            </div> 
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <p>
                                    <strong>Email: </strong>
                                <div>${email} </div><br>
                            </div> 
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <p>
                                    <strong>Telefonszám: </strong>
                                <div>${phonenumber} </div><br>
                            </div> 
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <p>
                                    <strong>Irányítószám: </strong>
                                <div>${postalcode} </div><br>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>