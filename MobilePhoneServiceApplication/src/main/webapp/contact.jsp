<%-- 
    Document   : contact
    Created on : Feb 25, 2020, 3:49:44 PM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0 bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class="row">
                <div class="col-lg-6">
                    <div class="mapouter"><div class="gmap_canvas"><iframe width="800" height="600" id="gmap_canvas" src="https://maps.google.com/maps?q=budapest%20klapka%20utca%2011&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://ytify.com"></a></div><style>.mapouter{position:relative;text-align:right;max-height:600px;max-width:800px;}.gmap_canvas {overflow:hidden;background:none!important;max-height:600px;max-width:800px;}</style></div>
                </div>
                <div class="col-lg-6">
                    <form method ="POST" class="pr-3" action="UserEmailServlet">
                        <h1 class="text-center my-5">
                            Írjon nekünk
                        </h1>

                        <div class="form-group">
                            <input class="form-control" type="text" name="name" required="required" placeholder="Név">
                        </div>

                        <div class="form-group">
                            <input class="form-control" type="email" name="email" required="required" placeholder="Email">
                        </div>

                        <div class="form-group">
                            <input class="form-control" type="text" name="subject" required="required" placeholder="Tárgy">
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="message" required="required" placeholder="Üzenet"></textarea>
                        </div>
                        <button class="btn btn-primary">
                            Küldés
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </button>
                    </form>
                     <c:if test="${email != null && userName != null}">
                    <c:out value="Kedves ${userName}! Köszönjük üzenetét! Hamarosan választ küldünk megadott ${email} e-mail címére."/>
                </c:if>
                </div>         
            </div>
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>