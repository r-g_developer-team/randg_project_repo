<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <script>
            $(".collapse").collapse()
        </script>
    </head>
    <body>
        <div class="container-fluid px-0">
            <%@include file="menu/menu.jsp" %>
            <div class="row" id="newWorksheet">
                <div class="col-lg-4">
                    <div class="card">

                        <h5 class="card-header info-color white-text text-center py-4">
                            <strong>Új hibajegy</strong>
                        </h5>

                        <div class="card-body px-lg-5 pt-0">

                            <form class="" style="color: #757575;" method="POST" action="WorkSheetServlet" enctype="multipart/form-data">

                                <div class="md-form mt-3">
                                    <label for="phoneType">Telefon típusa</label>
                                    <input id="phoneType" name="phoneType" type="text" class="form-control" required="required">

                                </div>

                                <div class="md-form">
                                    <label for="imeiNumber">IMEI száma</label>
                                    <input id="imeiNumber" name="imeiNumber" type="number" class="form-control" required="required">
                                </div>

                                <div class="md-form">
                                    <label for="issueDescription">Hiba leírása</label>
                                    <textarea name="issueDescription" class="form-control md-textarea" required="required" rows="3">
                                    </textarea>
                                </div>

                                <div class="md-form">
                                    <input name="image" type="file" id="file" multiple>                                
                                </div> 
                                <div class="md-form text-center">
                                    <button class="btn btn-outline-info btn-rounded z-depth-0 my-4 waves-effect"
                                            style="width: 70%;"
                                            type="submit">
                                        Küldés
                                    </button>
                                </div>    
                            </form>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <h5 class="card-header info-color white-text text-center py-4">
                            <strong>Hibajegyeim</strong>
                        </h5>
                    </div>    
                    <div class="accordion" id="accordion">
                        <c:forEach var="worksheet" items="${worksheets}">
                            <div class="card">
                                <div class="card-header bg-light"
                                     style="cursor: pointer;"
                                     data-toggle="collapse"
                                     data-target="#collapse-${worksheet.imeiNumber}"
                                     id="heading-${worksheet.imeiNumber}">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-bottom-0 border-top-0 border-left-0">
                                            <h1 class="float-left">
                                                <i class="fas fa-plus mr-4"></i>
                                            </h1>    
                                            <p class="float-left">
                                                <strong>Telefon típusa: </strong><br>
                                                <c:out value="${worksheet.phoneType}"/>                                         
                                            </p> 
                                        </div> 
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <p>
                                                <strong>IMEI száma: </strong><br>
                                                <c:out value="${worksheet.imeiNumber}"/>
                                            </p>            
                                        </div>
                                    </div>
                                </div>

                                <div id="collapse-${worksheet.imeiNumber}"
                                     class="collapse"
                                     aria-labelledby="heading-${worksheet.imeiNumber}"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                                <p>
                                                    <strong>Hiba leírása: </strong><br>
                                                </p>
                                                <p>
                                                    <c:out value="${worksheet.issueDescription}"/>
                                                </p>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                                <img src="${worksheet.imgPath}" alt="${worksheet.phoneType}" width="300">
                                            </div>    
                                        </div> 
                                    </div>
                                    <div class="card-body"> 
                                        <!-- Add class 'active' to progress -->
                                        <div class="row d-flex justify-content-center">
                                            <div class="col-12">
                                                <c:choose>
                                                    <c:when test = "${worksheet.worksheetStatus == 'WAITING_OFFER'}">
                                                        <ul id="progressbar" class="text-center">
                                                            <li class="active step0"></li>
                                                            <li class="step0"></li>
                                                            <li class="step0"></li>
                                                            <li class="step0"></li>
                                                        </ul>
                                                    </c:when>
                                                    <c:when test = "${worksheet.worksheetStatus == 'UNTIL_REPAIRE'}">
                                                        <ul id="progressbar" class="text-center">
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                            <li class="step0"></li>
                                                            <li class="step0"></li>
                                                        </ul>
                                                    </c:when>
                                                    <c:when test = "${worksheet.worksheetStatus == 'REPAIRE_COMPLETED'}">
                                                        <ul id="progressbar" class="text-center">
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                            <li class="step0"></li>
                                                        </ul>
                                                    </c:when>
                                                    <c:when test = "${worksheet.worksheetStatus == 'DONE'}">
                                                        <ul id="progressbar" class="text-center">
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                            <li class="active step0"></li>
                                                        </ul>
                                                    </c:when>
                                                </c:choose>    
                                            </div>
                                        </div>
                                        <div class="row justify-content-between top">
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/offer.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Árajánlat<br>készítése</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/repair.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Javítás<br>alatt</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/smiley.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Javítás<br>elkészült</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/tick.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Hibajegy<br>lezárva</p>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="EditWorksheetServlet" method="POST" class="text-center">
                                            <input type="hidden" name="worksheetId" value="${worksheet.workSheetId}">
                                            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
                                                <img class="icon m-3" size="10px" src="images/worksheet/edit.png">
                                            </button>
                                        </form>
                                    </div>        
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>    
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
