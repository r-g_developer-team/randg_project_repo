<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.jsp">
        <img class="logo" src="images/menu/rg.png" height="90px">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse m-2" id="navbarResponsive" style="font-size: 1.5em;">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.jsp">R�lunk</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="FeeServlet">�rlista</a>
            </li>
            <!--                <li class="nav-item">
                                <a class="nav-link" href="WorkSheetServlet">Szerv�z</a>
                            </li>-->
            <li class="nav-item">
                <a class="nav-link" href="contact.jsp">Kapcsolat</a>
            </li>
            <% if (request.getRemoteUser() == null) { %>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#login">
                    <img id="login-image" 
                         src="images/login.png"
                         width="35px"
                         > 
                </a>
            </li>
            <% } else {%>
            <% request.setAttribute("isUser", request.isUserInRole("user"));%>
            <c:if test="${requestScope.isUser}">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:dodgerblue" >
                            <%= request.getRemoteUser()%>
                        </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="WorkSheetServlet" class="dropdown-item">Szerv�z</a>
                                <a href="UserServlet" class="dropdown-item">Adatm�dos�t�s</a>
                                <div class="dropdown-divider"></div>
                                <form method="POST" action="LogoutServlet" id="logoutForm">
                                    <a href= 'javascript:document.getElementById("logoutForm").submit();'  class="dropdown-item">Kijelentkez�s</a>
                                </form>
                            </div>
                        </li>
                    </ul>
                </c:if>
                <c:if test="${!requestScope.isUser}">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:indianred" >
                                <%= request.getRemoteUser()%>
                            </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="AdminUserServlet" class="dropdown-item">Admin</a>
                            <div class="dropdown-divider"></div>
                            <form method="POST" action="LogoutServlet" id="logoutForm">
                                <a href= 'javascript:document.getElementById("logoutForm").submit();'  class="dropdown-item">Kijelentkez�s</a>
                            </form>
                        </div>
                    </li>
                </ul>
            </c:if>
            <% }%>

            </a>

        </ul>
    </div>
</nav>