<%-- 
    Document   : index
    Created on : Feb 17, 2020, 9:23:42 AM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0">
            <%@include file="menu/menu.jsp" %>
            <div class="row">
                <div class="col-lg-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="images/index/banner1.jpg" class="d-block w-100">
                            </div>
                            <div class="carousel-item">
                                <img src="images/index/banner2.jpg" class="d-block w-100">
                            </div>
                            <div class="carousel-item">
                                <img src="images/index/banner3.jpg" class="d-block w-100">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row p-5 mt-5">
                <div class="col-lg-3 text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="images/index/repair1.png" width="50%">
                        </div>  
                        <div class="col-lg-12 m-3 text-center" style="font-size: 1em;">
                            <p><strong>Meghibásodott eszköze, és szeretné megjavíttatni? 
                                    Önnek szerencséje van akkor, ha oldalunkra talált! Amennyiben nem rendelkezik még regisztrációval, 
                                    kattintson az oldal tetején lévő ikonra, és bátran tegye meg. Ezután a legördülő lehetőségek közül a szervíz oldalon
                                    tudja megadni hibás készülékének adatait. Kérjük, hogy amennyiben lehetséges, használja a képfeltöltés lehetőséget is, 
                                    melynek segítségével csapatunk könnyebben felmérik a hibát. Amennyiben bármilyen kérdése felmerülne, bátran írjon nekünk a kapcsolatok
                                    fül alatt található e-mail küldő funkció segítségével.</strong></p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="images/index/browsing.png" width="50%">
                        </div> 
                        <div class="col-lg-12 m-3 text-center" style="font-size: 1em;">
                            <p> <strong>Miután felvette hibás készülékének adatait, három munkanapon belül válaszolunk Önnek. Csapatunk ez idő alatt
                                    igyekszik minél gyorsabban felmérni a hiba nagyságát, és megoldási javaslatokat dolgoznak ki, melyet árajánlat formájában 
                                    továbbítunk Önnek regisztrációjához rendelve, ezt szintén a szervíz fülre kattintva tekintheti meg. </strong></p>
                        </div>
                    </div>    
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="images/index/repair2.png" width="50%">
                        </div>  
                        <div class="col-lg-12 m-3 text-center" style="font-size: 1em;">
                            <p><strong>Mindannyian tudjuk, hogy manapság az informatika és a hozzá kapcsolódó eszközök 
                                    milyen nagy mértékben határozzák meg mindennapjainkat. Éppen ezért Az árajánlat 
                                    kétoldali elfogadása után kollégáink azonnal megkezdik munkájukat. Mikor elkészültünk 
                                    e-mailben küldünk Önnek értesítőt, és nyitvatrtási idő alatt készülékét bármikor átveheti.</strong></p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="images/index/happy.png" width="50%">
                        </div> 
                        <div class="col-lg-12 text-center" style="font-size: 1em;">
                            <p><strong>Ügyfeleink elégedettsége rendkívül fontos számunkra. Hiszen a konstruktív visszajelzések segítik leginkább cégünk fejlődését
                                    annak érdekében, hogy ügyfeleinknek minél minőségibb szolgáltatásokat tudjunk nyújtani. Ez állandó jellegű átgondolást, tervezést
                                    igényel. Hálásan vesszük minden kedves Ügyfelünk visszajelzését akár személyesen beszélgetés, akár telefonhívás, akár e-mail üzenet formájában.</strong></p>
                        </div>
                    </div> 
                </div>
            </div>      
            <div class="index-services">
                <h1><span>Mobiltelefon</span></h1>
                <div class="content">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center">
                            <img src="images/index/iphone.png" width="80%">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center" style="font-size: 1.3em;">
                            <p>Mobiltelefonjának kijelzője tört be? Az akkumulátor nem úgy működik már, mint régen? 
                                A készülék lajhár-lassúságra kapcsolt? Vagy csak a legújabb stílusú fedőlapot szeretné használni?
                                Az árlista fül alatt bővebben tájékozódhat, további lehetőségeket is találva!</p>
                        </div>
                    </div>
                </div>
                <h1><span>Tablet</span></h1>
                <div class="content">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center" style="font-size: 1.3em;">
                            <p>A tabletek korát kezdjük élni. Sok cég a legkülönfélébb módokon használtatja alkalmazottaival.
                                Tipikus hibák sokaságát kezeltük már eredményesen. Tekintse meg árlistánkat, melyet az azonos nevű fülre kattintva találhat meg.</p>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center">
                            <img src="images/index/ipad.png" width="60%">
                        </div>
                    </div>                    
                </div>
                <h1><span>Számítógép</span></h1>
                <div class="content">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center">
                            <img src="images/index/imac.png" width="70%">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 align-self-center text-center" style="font-size: 1.3em;">
                            <p>A számítógép mindennapos használati tárgy. Legyen szó felnőttek munkavégzésének eszközéről, vagy diákok tanulásáról. A legszerteágazóbb
                                problémákkal találtuk munkáink során szembe magunkat, ezzel tapasztalatainkat sikeresen bővítettük.
                                A rendszeres használat során egy portalanítás igazi újjászületés egy számítógép életében a legtöbb szakember
                                évente egyszer ajánlja, főleg laptopok esetében. Monitor csere, operációs rendszer újratelepítése,
                                vírusírtók és további szoftverek telepítése, hardveres problémák orvosolása a leggyakoribb. A teljes szolgáltatás 
                                szintén megtalálható az árlista fül alatt.</p>
                        </div>
                    </div>                    
                </div>      
            </div> 
            <!-- Footer -->
            <footer class="page-footer font-small bg-info mt-2" style="padding-top: 100px;">

                <!-- Footer Text -->
                <div class="container-fluid text-center text-md-left">

                    <!-- Grid row -->
                    <div class="row">

                        <!-- Grid column -->
                        <div class="col-lg-6 mt-md-0 mt-3 text-center">

                            <!-- Content -->
                            <h5 class="text-uppercase font-weight-bold">A csapat</h5>
                            <p>
                                Leopold Róbert </br>
                                Molnár Máté </br>
                                Ábel György </br>
                                Kövécs Levente
                            </p>

                        </div>
                        <!-- Grid column -->

                        <hr class="clearfix w-100 d-md-none pb-3">

                        <!-- Grid column -->
                        <div class="col-lg-6 mb-md-0 mb-3">

                            <!-- Content -->
                            <h5 class="text-uppercase font-weight-bold">
                                Üzletünk nyitvatartási ideje
                            </h5>
                            <table class="table table-borderless"> 
                                <tr>
                                    <td>
                                        Hétfő
                                    </td>
                                    <td>
                                        8:00-20:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Kedd
                                    </td>
                                    <td>
                                        10:00-20:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Szerda
                                    </td>
                                    <td>
                                        12:00-20:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Csütörtök
                                    </td>
                                    <td>
                                        10:00-18:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Péntek
                                    </td>
                                    <td>
                                        10:00-18:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Szombat
                                    </td>
                                    <td>
                                        8:00-12:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Vasárnap
                                    </td>
                                    <td>
                                        8:00-8:10
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                </div>
                <!-- Footer Text -->

                <!-- Copyright -->
                <div class="footer-copyright text-center py-3">
                    © 2020 Copyright: Dream Team :P
                </div>
                <!-- Copyright -->

            </footer>
            <!-- Footer -->
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
