<%-- 
    Document   : registration
    Created on : Mar 2, 2020, 3:17:38 PM
    Author     : momate
--%>


<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0 bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-8 col-lg-8 col-xl-6">
                        <div class="row">
                            <div class="col text-center">
                                <p/>
                                <h1>Regisztráció</h1>

                            </div>
                        </div>
                        <form method="POST" action="RegistrationServlet">
                            <div class="row align-items-center">
                                <div class="col mt-4">
                                    <input name="lastName" type="text" class="form-control" placeholder="Vezetéknév" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col mt-4">
                                    <input name="firstName" type="text" class="form-control" placeholder="Keresztnév" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center mt-4">
                                <div class="col">
                                    <input name="phoneNumber" type="text" class="form-control" placeholder="Telefonszám" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center mt-4">
                                <div class="col">
                                    <input name="postalCode" type="text" class="form-control" placeholder="Irányítószám" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center mt-4">
                                <div class="col">
                                    <input name="city" type="text" class="form-control" placeholder="Város" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center mt-4">
                                <div class="col">


                                    <font color="red"><%=(request.getAttribute("errMessage") == null) ? ""
                                            : request.getAttribute("errMessage")%></font>
                                    <input name="email" type="text" class="form-control" placeholder="E-mail" required="required">
                                </div>
                            </div>
                            <div class="row align-items-center mt-4">
                                <div class="col">
                                    <input name="psw" type="password" class="form-control" placeholder="Jelszó" required="required">
                                </div>
                            </div>
                            <div class="row justify-content-start mt-4">
                                <div class="col">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" required="required">
                                            Elolvastam és elfogadtam az <a href=aszf.html>Általános Szerződési Feltételeket</a>
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-primary mt-4">Regisztrálok</button>
                                    <p/>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
