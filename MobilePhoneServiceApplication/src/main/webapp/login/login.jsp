<%-- 
    Document   : login
    Created on : Mar 2, 2020, 1:34:40 PM
    Author     : george
--%>

<!-- The Modal -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal" id="login">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Bejelentkezés</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="j_security_check" method="POST">
                    <div class="form-group">
                        <label for="usr">E-mail cím</label>
                        <input type="text" class="form-control" placeholder="E-mail cím" id="username" name="j_username" required="required">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Jelszó</label>
                        <input type="password" class="form-control" placeholder="Jelszó" id="password" name="j_password" required="required">
                    </div> 
                    <div class="row">
                        <div class="col-6 text-center">
                            
                            <button type="submit" value="login" class="btn btn-info">Bejelentkezés</button>
                            
                        </div>
                        <div class="col-6 text-center">
                                <a href="registration.jsp">
                                    <button type="button" class="btn btn-warning">Regisztráció</button>
                                </a>
                        </div>

                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <div class="row" style="width: 100%;">
                    <div class="col-12 text-center">
                        <a href="forgotPassword.jsp" id="pwReset">
                            <button type="button" class="btn btn-outline-danger">
                                Elfelejtettem a jelszavam
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
