<!-- Footer -->
<footer class="page-footer font-small bg-info mt-2" style="padding-top: 100px;">

    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-lg-6 mt-md-0 mt-3 text-center">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold">A csapat</h5>
                <p>
                    Leopold R�bert </br>
                    Moln�r M�t� </br>
                    �bel Gy�rgy </br>
                    K�v�cs Levente
                </p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-lg-6 mb-md-0 mb-3">

                <!-- Content -->
                <h5 class="text-uppercase font-weight-bold">
                    �zlet�nk nyitvatart�si ideje
                </h5>
                <table class="table table-borderless"> 
                    <tr>
                        <td>
                            H�tf?
                        </td>
                        <td>
                            8:00-20:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Kedd
                        </td>
                        <td>
                            10:00-20:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Szerda
                        </td>
                        <td>
                            12:00-20:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cs�t�rt�k
                        </td>
                        <td>
                            10:00-18:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            P�ntek
                        </td>
                        <td>
                            10:00-18:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Szombat
                        </td>
                        <td>
                            8:00-12:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vas�rnap
                        </td>
                        <td>
                            8:00-8:10
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Text -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        � 2020 Copyright: Dream Team :P
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->