<%-- 
    Document   : adminworksheeteditor
    Created on : 26-Mar-2020, 14:42:39
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header info-color white-text text-center py-4">
                                    <strong>Státusz frissítése</strong>
                                </h5>
                                <div class="card-body row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-top-0 border-left-0">
                                        <p>
                                            <strong>Telefon típusa: </strong><br>
                                            <c:out value="${worksheet.phoneType}"/>                                         
                                        </p>            
                                    </div> 
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                        <p>
                                            <strong>IMEI száma: </strong><br>
                                            <c:out value="${worksheet.imeiNumber}"/>
                                        </p>            
                                    </div> 
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <br>
                                        <p>
                                            <strong>Hiba leírása: </strong><br>
                                            <c:out value="${worksheet.issueDescription}"/>
                                        </p>            
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <form class="" id="editWorksheet" style="color: #757575;" method="POST" action="AdminWorksheetStatusServlet">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <select class="form-control mt-3 mb-3" style="display: inline-block !important; width: 50%; text-align-last:center;" id="status" name="worksheetStatus">
                                                        <c:choose>
                                                            <c:when test = "${worksheet.worksheetStatus == 'WAITING_OFFER'}">
                                                                <option selected="selected" value="WAITING_OFFER">Árajánlat készítése</option>
                                                                <option value="UNTIL_REPAIRE">Javítás alatt</option>
                                                                <option value="REPAIRE_COMPLETED">Javítás elkészült</option>
                                                                <option value="DONE">Hibajegy lezárva</option>
                                                            </c:when>
                                                            <c:when test = "${worksheet.worksheetStatus == 'UNTIL_REPAIRE'}">
                                                                <option value="WAITING_OFFER">Árajánlat készítése</option>
                                                                <option selected="selected" value="UNTIL_REPAIRE">Javítás alatt</option>
                                                                <option value="REPAIRE_COMPLETED">Javítás elkészült</option>
                                                                <option value="DONE">Hibajegy lezárva</option>
                                                            </c:when>
                                                            <c:when test = "${worksheet.worksheetStatus == 'REPAIRE_COMPLETED'}">
                                                                <option value="WAITING_OFFER">Árajánlat készítése</option>
                                                                <option value="UNTIL_REPAIRE">Javítás alatt</option>
                                                                <option selected="selected" value="REPAIRE_COMPLETED">Javítás elkészült</option>
                                                                <option value="DONE">Hibajegy lezárva</option>
                                                            </c:when>
                                                            <c:when test = "${worksheet.worksheetStatus == 'DONE'}">
                                                                <option value="WAITING_OFFER">Árajánlat készítése</option>
                                                                <option value="UNTIL_REPAIRE">Javítás alatt</option>
                                                                <option value="REPAIRE_COMPLETED">Javítás elkészült</option>
                                                                <option selected="selected" value="DONE">Hibajegy lezárva</option>
                                                            </c:when>
                                                        </c:choose>
                                                    </select>
                                                </div>    
                                            </div>    
                                            <div class="md-form">
                                                <input type="hidden" name="worksheetId" value="${worksheet.workSheetId}">
                                                <input type="hidden" name="imeiNumber" value="${worksheet.imeiNumber}">
                                                <input type="hidden" name="phoneType" value="${worksheet.phoneType}">
                                                <input type="hidden" name="issueDescription" value="${worksheet.issueDescription}">
                                                <input type="hidden" name="imgPath" value="${worksheet.imgPath}">
                                                <input type="hidden" name="ticketCreationDate" value="${worksheet.ticketCreationDate}">
                                                <div class="row">
                                                    <div class="col text-center">
                                                        <button 
                                                            name="update"
                                                            class="btn btn-outline-info btn-rounded z-depth-0 mt-4 waves-effect"
                                                            style="width: 50%;"
                                                            type="submit">Státusz frissítése
                                                        </button>
                                                        <button 
                                                            name="delete" 
                                                            class="btn btn-outline-danger btn-rounded z-depth-0 mt-4 mb-4 waves-effect" 
                                                            style="width: 50%;" 
                                                            type="submit"
                                                            onclick="confirmRemove(event)">
                                                            Hibajegy törlése
                                                        </button>
                                                    </div>
                                                </div>
                                                <script>
                                                    function confirmRemove(e) {
                                                        var r = confirm("Biztosan törli a hibajegyet?");
                                                        if (r == false) {
                                                            e.preventDefault();
                                                            e.stopPropagation();
                                                        }
                                                    }
                                                </script>
                                            </div>    
                                        </form>
                                    </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <c:choose>
                                            <c:when test = "${worksheet.worksheetStatus == 'WAITING_OFFER'}">
                                                <ul id="progressbar" class="text-center">
                                                    <li class="active step0"></li>
                                                    <li class="step0"></li>
                                                    <li class="step0"></li>
                                                    <li class="step0"></li>
                                                </ul>
                                            </c:when>
                                            <c:when test = "${worksheet.worksheetStatus == 'UNTIL_REPAIRE'}">
                                                <ul id="progressbar" class="text-center">
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                    <li class="step0"></li>
                                                    <li class="step0"></li>
                                                </ul>
                                            </c:when>
                                            <c:when test = "${worksheet.worksheetStatus == 'REPAIRE_COMPLETED'}">
                                                <ul id="progressbar" class="text-center">
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                    <li class="step0"></li>
                                                </ul>
                                            </c:when>
                                            <c:when test = "${worksheet.worksheetStatus == 'DONE'}">
                                                <ul id="progressbar" class="text-center">
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                    <li class="active step0"></li>
                                                </ul>
                                            </c:when>
                                        </c:choose> 
                                        <div class="row justify-content-between top">
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/offer.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Árajánlat<br>készítése</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/repair.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Javítás<br>alatt</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> 
                                                <img class="icon" src="images/worksheet/smiley.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Javítás<br>elkészült</p>
                                                </div>
                                            </div>
                                            <div class="row d-flex icon-content"> <img class="icon" src="images/worksheet/tick.png">
                                                <div class="d-flex flex-column">
                                                    <p class="font-weight-bold">Hibajegy<br>lezárva</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div> 
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <img src="${worksheet.imgPath}" width="100%">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->   

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>

