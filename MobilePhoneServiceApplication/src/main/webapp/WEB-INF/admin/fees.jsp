<%-- 
    Document   : index
    Created on : 24-Mar-2020, 14:11:06
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <table id="fees" class="table" style="width:100%">
                        <thead>
                            <tr align="center">
                                <th>Megnevezés</th>
                                <th>Ár</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="f" items="${fees}">
                                <tr align="center">
                                    <td><c:out value="${f.feeName}"/></td>
                                    <td><c:out value="${f.price}"/></td>
                                    <td>
                                        <form action="AdminFeeServlet" method="POST" class="text-center" name="from1">
                                            <input type="hidden" name="feeId" value="${f.feeId}">
                                            <button type="submit" class="close" style="float: none !important;">
                                                <img class="icon" width="20px" src="images/worksheet/edit.png">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>                   
                        </tbody>
                    </table>
                    <form action="AdminFeeServlet" method="POST" class="text-center" name="form2">
                        <input type="hidden" name="feeId" value="0">
                        <button type="submit" class="btn btn-primary" style="float: right;">Hozzáadás</button>
                    </form>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!--        <script>
                    $(document).ready(function () {
                        $('#fees').DataTable();
                    });
                </script>    -->

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
