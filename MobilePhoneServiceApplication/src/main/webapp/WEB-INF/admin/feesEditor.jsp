<%-- 
    Document   : feesEditor
    Created on : 2020.03.26., 17:46:26
    Author     : momate
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            textarea {
                resize: none;
            }
        </style>
    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header info-color white-text text-center py-4">
                                    <strong>Szerkesztés</strong>
                                </h5>
                                <div class="card-body row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-top-0 border-left-0">
                                        <p>
                                            <strong>Megnevezés</strong><br>

                                            <textarea rows="1" cols="20" name="feeName" form="editFee" required="required"> <c:out value="${fee.feeName}"/></textarea>                                     
                                        </p>            
                                    </div> 
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                        <p>
                                            <strong>Ár</strong><br>
                                            <textarea rows="1" cols="20" name="feePrice" form="editFee" required="required"> <c:out value="${fee.price}"/></textarea>

                                        </p>            
                                    </div> 

                                </div>    
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <form class="" id="editFee" style="color: #757575;" method="POST" action="AdminFeeUpdateServlet">
                                            <div class="md-form">
                                                <input type="hidden" name="feeId" value="${fee.feeId}">

                                                <div class="row">
                                                    <div class="col text-center">

                                                        <button 
                                                            name="add"
                                                            class="btn btn-outline-success btn-rounded z-depth-0 mt-4 waves-effect"
                                                            style="width: 50%;"
                                                            type="submit">Hozzáadás
                                                        </button>
                                                        <button 
                                                            name="update"
                                                            class="btn btn-outline-info btn-rounded z-depth-0 mt-4 waves-effect"
                                                            style="width: 50%;"
                                                            type="submit" onclick="confirmEdit(event)">
                                                            Frissítés
                                                        </button>
                                                        <button 
                                                            name="delete" 
                                                            class="btn btn-outline-danger btn-rounded z-depth-0 mt-4 mb-4 waves-effect" 
                                                            style="width: 50%;" 
                                                            type="submit"
                                                            onclick="confirmRemove(event)">
                                                            Törlés
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>    
                                        </form>
                                    </div>
                                </div>    
                            </div> 
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->   

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            function confirmRemove(e) {
                var r = confirm("Biztosan törli az árat?");
                if (r == false) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        </script>
        <script>
            function confirmEdit(e) {
                var r = confirm("Biztosan megváltoztatja az árat?");
                if (r == false) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        </script>


    </body>
</html>


