<%-- 
    Document   : index
    Created on : 24-Mar-2020, 14:11:06
    Author     : george
--%>

<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body onload="javascript:check()">
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <table id="fees" class="table" style="width:100%">
                        <thead>
                            <tr align="center">
                                <th><span>Jog</span></th>
                                <th><span>Vezetéknév</span></th>
                                <th><span>Keresztnév</span></th>
                                <th><span>E-mail</span></th>
                                <th><span>Telefonszám</span></th>
                                <th><span>Irányítószám</span></th>
                                <th><span>Város</span></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="u" items = "${usersList}"> 
                                <tr align = "center">
                                    <c:choose>
                                        <c:when test="${u.admin}">
                                            <td> <img class="icon" width="20px" src="images/users/god.png"> </td>
                                            </c:when>
                                            <c:otherwise>
                                            <td> <img class="icon" width="20px" src="images/users/user.png"></td>
                                            </c:otherwise>
                                        </c:choose>
                                    <td><c:out value="${u.firstName}"/></td>
                                    <td><c:out value="${u.lastName}"/></td>
                                    <td><c:out value="${u.email}"/></td>
                                    <td><c:out value="${u.phoneNumber}"/></td>
                                    <td><c:out value="${u.postalCode}"/></td>
                                    <td><c:out value="${u.city}"/></td>
                                    <td align="center">
                                        <div class="row">                                           
                                            <div class="col-6">

                                                <form action="AdminUserServlet" method="POST" name="form1">
                                                    <input type="hidden" name="userId" value="${u.userId}">
                                                    <button type="submit" class="close" style="float: none !important;">
                                                        <img class="icon" width="20px" src="images/worksheet/edit.png">
                                                    </button>
                                                </form>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>                   
                        </tbody>
                    </table>

                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->


        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
