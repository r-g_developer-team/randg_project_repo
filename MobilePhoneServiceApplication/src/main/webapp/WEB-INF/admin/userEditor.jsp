<%-- 
    Document   : userEditor
    Created on : 2020.03.28., 22:53:28
    Author     : momate
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            textarea {
                resize: none;
            }
        </style>
    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <form class="" id="editUser" style="color: #757575;" method="POST" action="AdminUserEditorServlet">
                                <div class="card">
                                    <h5 class="card-header info-color white-text text-center py-4">
                                        <strong>Szerkesztés</strong>
                                    </h5>

                                    <div class="card-body row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-top-0 border-left-0">
                                            <p>
                                                <strong>Vezetéknév</strong><br>

                                                <textarea rows="1" cols="30" name="userFName" form="editUser" required="required"><c:out value="${user.firstName}"/></textarea>                                     
                                            </p>            
                                        </div> 
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                            <p>
                                                <strong>Keresztnév</strong><br>
                                                <textarea rows="1" cols="30" name="userLName" form="editUser" required="required"><c:out value="${user.lastName}"/></textarea>

                                            </p>            
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                            <p>
                                                <strong>Email</strong><br>
                                                <textarea rows="1" cols="30" name="email" form="editUser" required="required"><c:out value="${user.email}"/></textarea>
                                                <input type="hidden" name="origin" value="${user.email}">

                                            </p>            
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                            <p>
                                                <strong>Telefonszám</strong><br>
                                                <textarea rows="1" cols="30" name="phone" form="editUser" required="required"><c:out value="${user.phoneNumber}"/></textarea>

                                            </p>            
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                            <p>
                                                <strong>Irányítószám</strong><br>
                                                <textarea rows="1" cols="30" name="postal" form="editUser" required="required"><c:out value="${user.postalCode}"/></textarea>

                                            </p>            
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">
                                            <p>
                                                <strong>Város</strong><br>
                                                <textarea rows="1" cols="30" name="city" form="editUser" required="required"><c:out value="${user.city}"/></textarea>

                                            </p>            
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border border-left-0 border-top-0 border-right-0">

                                            <strong>Jog</strong><br>
                                            <select class="browser-default custom-select" style="width: 150px" id="roles" name="roles" title="Válassz jogot...">
                                                <option value="user">User</option>
                                                <option value="admin">Admin</option>
                                            </select>

                                      
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                                            <div class="md-form">
                                                <input type="hidden" name="userId" value="${user.userId}">
                                                <input type="hidden" name="pw" value="${user.password}">


                                                <div class="row">
                                                    <div class="col text-center">
                                                        <button 
                                                            name="update"
                                                            class="btn btn-outline-info btn-rounded z-depth-0 mt-4 waves-effect"
                                                            style="width: 50%;"
                                                            type="submit" onclick="confirmEdit(event)">
                                                            Frissítés
                                                        </button>
                                                        <button 
                                                            name="delete" 
                                                            class="btn btn-outline-danger btn-rounded z-depth-0 mt-4 mb-4 waves-effect" 
                                                            style="width: 50%;" 
                                                            type="submit"
                                                            onclick="confirmRemove(event)">
                                                            Törlés
                                                        </button>

                                                    </div>
                                                </div>

                                            </div>    

                                        </div>
                                    </div>    
                                </div> 
                            </form>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->   

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

        <script>
            function confirmEdit(e) {
                var r = confirm("Biztosan megváltoztatja az adatokat?");
                if (r == false) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        </script>
        <script>
            function confirmRemove(e) {
                var r = confirm("Biztosan törli a felhasználót?");
                if (r == false) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        </script>


    </body>
</html>
