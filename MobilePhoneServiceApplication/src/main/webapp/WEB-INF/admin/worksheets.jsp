<%-- 
    Document   : worksheets
    Created on : 24-Mar-2020, 14:11:06
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div class="d-flex" id="wrapper">

            <%@include file="sidebar/sidebar.jsp" %>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-primary" id="menu-toggle">Admin menü</button>
                </nav>

                <div class="container-fluid">
                    <table id="worksheets" class="table" style="width:100%">
                        <thead>
                            <tr align="center">
                                <th>Telefon típusa</th>
                                <th>IMEI száma</th>
                                <th>Létrehozás dátuma</th>
                                <th>Státusz</th>
                                <th>Szerkesztés</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="worksheet" items="${worksheets}">
                                <tr align="center">
                                    <td><c:out value="${worksheet.phoneType}"/></td>
                                    <td><c:out value="${worksheet.imeiNumber}"/></td>
                                    <td><c:out value="${worksheet.ticketCreationDate}"/></td>
                                    <c:choose>
                                        <c:when test = "${worksheet.worksheetStatus == 'WAITING_OFFER'}">
                                            <td>Árajánlatra vár</td>
                                        </c:when>
                                        <c:when test = "${worksheet.worksheetStatus == 'UNTIL_REPAIRE'}">
                                            <td>Javítás alatt</td>
                                        </c:when>
                                        <c:when test = "${worksheet.worksheetStatus == 'REPAIRE_COMPLETED'}">
                                            <td>Javítás elkészült</td>
                                        </c:when>
                                        <c:when test = "${worksheet.worksheetStatus == 'DONE'}">
                                            <td>Hibajegy lezárva</td>
                                        </c:when>
                                    </c:choose>    
                                    <td>
                                        <form action="AdminWorksheetServlet" method="POST" class="text-center">
                                            <input type="hidden" name="worksheetId" value="${worksheet.workSheetId}">
                                            <button type="submit" class="close" style="float: none !important;">
                                                <img class="icon" width="20px" src="images/worksheet/edit.png">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <script>
            $(document).ready(function () {
                $('#worksheets').DataTable();
            });
        </script>    

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
