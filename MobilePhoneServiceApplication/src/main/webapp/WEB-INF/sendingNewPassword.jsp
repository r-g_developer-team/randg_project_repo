<%-- 
    Document   : sendingNewPassword
    Created on : 2020.03.21., 16:48:00
    Author     : levsz
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../meta-datas/meta.jsp" %>
        <%@include file="../header/header.jsp" %>
        <title>Új jelszó küldése</title>
    </head>
    <body>
        <div class="container-md bg-light">
            <%@include file="../menu/menu.jsp" %>
            <div class = "text-center">
                <h1>Új jelszava úton van megadott e-mail címére!</h1>
                <c:out value = "${message}"/>
                <a class="btn btn-primary" href="index.jsp" role="button">Vissza</a>
            </div>
            <%@include file="../footer/footer.jsp" %>
            <%@include file="../login/login.jsp" %>
        </div>
    </body>
</html>
