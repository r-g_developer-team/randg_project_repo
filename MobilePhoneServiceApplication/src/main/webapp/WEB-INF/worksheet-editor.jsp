<%-- 
    Document   : worksheet-editor
    Created on : 20-Mar-2020, 15:51:42
    Author     : george
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../meta-datas/meta.jsp" %>
        <%@include file="../header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0">
            <%@include file="../menu/menu.jsp" %>
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12">
                    <div class="card">
                        <h5 class="card-header info-color white-text text-center py-4">
                            <strong>Adatok frissítése</strong>
                        </h5>

                        <div class="card-body px-lg-5 pt-0">

                            <form class="" id="editWorksheet" style="color: #757575;" method="POST" action="UpdateWorksheetServlet">

                                <div class="md-form mt-3">
                                    <label for="phoneType">Telefon típusa</label>
                                    <input name="phoneType" 
                                           type="text" 
                                           class="form-control" 
                                           required="required" 
                                           value="<c:out value='${worksheet.phoneType}'/>"
                                           />

                                </div>

                                <div class="md-form">
                                    <label for="imeiNumber">IMEI száma</label>
                                    <input name="imeiNumber" 
                                           type="text" 
                                           class="form-control" 
                                           required="required" 
                                           value="<c:out value='${worksheet.imeiNumber}'/>"
                                           />
                                </div>

                                <div class="md-form">
                                    <label for="issueDescription">Hiba leírása</label>
                                    <textarea name="issueDescription" class="form-control md-textarea" required="required" rows="3">
                                        <c:out value='${worksheet.issueDescription}'/>
                                    </textarea>
                                </div>
                                <div class="md-form">
                                    <input type="hidden" name="worksheetId" value="${worksheet.workSheetId}">
                                    <input type="hidden" name="imgPath" value="${worksheet.imgPath}">
                                    <input type="hidden" name="ticketCreationDate" value="${worksheet.ticketCreationDate}">
                                    <input type="hidden" name="worksheetStatus" value="${worksheet.worksheetStatus}">
                                    <div class="row">
                                        <div class="col text-center">
                                            <button 
                                                name="update"
                                                class="btn btn-outline-info btn-rounded z-depth-0 mt-4 waves-effect"
                                                style="width: 50%;"
                                                type="submit">Adatok frissítése
                                            </button>
                                            <button 
                                                name="delete" 
                                                class="btn btn-outline-danger btn-rounded z-depth-0 mt-4 mb-4 waves-effect" 
                                                style="width: 50%;" 
                                                type="submit"
                                                onclick="confirmRemove(event)">
                                                Hibajegy törlése
                                            </button>
                                        </div>
                                    </div>
                                    <script>
                                        function confirmRemove(e) {
                                            var r = confirm("Biztosan törli a hibajegyet?");
                                            if (r == false) {
                                                e.preventDefault();
                                                e.stopPropagation();
                                            }
                                        }
                                    </script>
                                </div>    
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
            <%@include file="../footer/footer.jsp" %>
            <%@include file="../login/login.jsp" %>
        </div> 
    </body>
</html>

