<%-- 
    Document   : forgotPassword
    Created on : 2020.03.21., 15:11:57
    Author     : levsz
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
        <title>Elfelejtett jelszó</title>
    </head>
    <body>
        <div class="container-md bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class = "text-center">
            <h3>Elfelejtett jelszó esetén újat küldünk megadott e-mail címére.</h3>
        <form method = "GET" action = "ForgotPasswordServlet">
            <fieldset>
                <label for="email">Email-cím:</label>
                <input id ="email" name ="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required />
               <!-- <input type="email" class="form-control" id="email" required > <br/> -->
                <button type="submit" class="btn btn-succes">Új jelszó kérése</button>
            </fieldset>
        </form>
            </div>
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
