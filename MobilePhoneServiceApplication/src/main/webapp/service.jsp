<%-- 
    Document   : service
    Created on : Feb 25, 2020, 3:37:22 PM
    Author     : George
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0 bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class="row">
                <div class="col-lg-4"> 
                    <h2>Árlista</h2>
                    <table class="table table-striped table-dark">
                        <c:forEach var="fee" items="${fees}">
                            <tr>
                                <td>
                                    <c:out value="${fee.getFeeName()}"/>
                                </td>
                                <td>
                                    <c:out value="${fee.getPrice()}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-lg-8">
                    
                </div>
            </div>
            <%@include file="footer/footer.jsp" %>
            <%@include file="login/login.jsp" %>
        </div>
    </body>
</html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
