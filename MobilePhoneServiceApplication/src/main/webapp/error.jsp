<%-- 
    Document   : error
    Created on : 2020.03.21., 21:20:32
    Author     : momate
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/errorPage.css">
        <title>Sikertelen belépés</title>
    </head>
    <body>
        <div class ="centered-content">
            <img src="images/wrongpw.jpg" alt="Hibás e-mail vagy jelszó.">
            <h4 class="error-text">A megadott felhasználónév vagy jelszó hibás!</h4> </br> </br>
            <a href="index.jsp" class="button" role="button">Vissza a főoldalra</a>
        </div>

    </body>
</html>
