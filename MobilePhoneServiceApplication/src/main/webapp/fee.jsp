<%-- 
    Document   : fee
    Created on : 2020. márc. 13., 18:39:55
    Author     : Leo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="meta-datas/meta.jsp" %>
        <%@include file="header/header.jsp" %>
    </head>
    <body>
        <div class="container-fluid px-0 bg-light">
            <%@include file="menu/menu.jsp" %>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-3 col-xl-3 text-center p-2"> 
                    <h2>Árlista</h2>
                    <table class="table text-center">
                        <c:forEach var="fee" items="${fees}">
                            <tr>
                                <td>
                                    <c:out value="${fee.getFeeName()}"/>
                                </td>
                                <td>
                                    <c:out value="${fee.getPrice()}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-12 col-md-12 col-lg-9 col-xl-9 text-center fees">
                    <h1 class="text-center m-5">Szolgáltatásaink</h1>
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <div class="box mb-2">
                                <img class="img-fluid" src="images/fees/display_replacement.jpg">
                                <div class="box-content text-white" style="font-size: 1.3em;">
                                    <h2 class="text-left m-4">Kijelző csere</h2><br>
                                    <p>Kijelzők cseréjét tekintve különös módon jutottunk nagyfokú tapasztalatszerzésre.
                                        A kezdetek kezdetén az emberek jobban vigyáztak készülékeikre, így magunk kellett kalapács segítségével 
                                        munkát generálni. Azóta már az emberek maguk is gondoskodnak róla. Egy jól sikerült buli után
                                        másnap, az ügyfelek kétségbeesetten jönnek betört kijelzőjükkel. Az örömünk ezután annak látványában
                                        rejlik, hogy 3-4 nap után milyen boldogan, és megkönnyebülve viszik haza "jobb, mint újkorában" és hasonló jelzőkkel illetett készüléküket.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <div class="box mb-2">
                                <img class="img-fluid" src="images/fees/battery_replacement.jpeg">
                                <div class="box-content text-white" style="font-size: 1.3em;">
                                    <h2 class="text-left m-4">Akkumulátor csere</h2><br>
                                    <p>Az "akkumulátorok működési elve az egyszerű galvánelemével megegyezik". Tanultuk, és kísérletek sorozatával
                                        tapasztaltuk is az előbbi kijelentést fizika órákon. Mi hiszünk a későbbi "fizkém" tantárgy egyetemi tananyagaiban is.
                                        Igyekszünk külön erre a célra kialakított laboratóriumunkban csiszolni ezen tudásunkat is. Az árak viszonylagos olcsóságának
                                        hátterében pontosan az e téren megszerzett tudásunk áll.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <div class="box mt-2">
                                <img class="img-fluid" src="images/fees/water_damage.jpg">
                                <div class="box-content text-white p-3" style="font-size: 1.3em;">
                                    <h2 class="text-left m-4">Ultrahangos tisztítás</h2><br>
                                    <p>Az ultrahangot a delfineken és a denevéreken kívül laborunkban mi is használjuk. Természetesen nem táplálékszerzés céljából,
                                        hanem munkánk elvégzésének megsegítésére. Ez a technológia, bár manapság már nem mondható kifejezetten egyedülállónak,
                                        mégis kevés üzlet mondhatja el magáról, hogy rendelkezik vele. Segítségével munkánk hatásfoka felülről súrolja a 100%-ot.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 col-xl-6 text-center">
                            <div class="box mt-2">
                                <img class="img-fluid" src="images/fees/motherboard_repaire.jpg">
                                <div class="box-content text-white p-3" style="font-size: 1.3em;">
                                    <h2 class="text-left m-4">Alplap javítás</h2><br>
                                    <p>Az alaplap javítása alatt értendő az évi egyszer ajánlott tisztítás, és a nyomtatott áramkörök karbantartása.
                                        Elektronikai munkatársunk igen lelkes: már munkaidő előtt, az M3-as metrón elkezdi forrasztani előző nap hazavitt nyák-jait.</p>
                                </div>
                            </div>
                        </div>  
                    </div> 
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12 col-xl-12 text-center">
                            <div class="box mt-3">
                                <img class="img-fluid" src="images/fees/laptop_cleaning.jpg">
                                <div class="box-content text-white p-3" style="font-size: 1.3em;">
                                    <h2 class="text-left m-4">Számítógép tisztítás</h2><br>
                                    <p>Számítógépek tisztítása évente egyszer ajánlott. Több alkalommal fordult már elő olyan eset, hogy 
                                        ügyfelünk ezt nem tartotta be. Ennek következtében a javítás sem egyszerű. Természetesen ez sem
                                        akadályozza meg a profi működésünket, de mi is a következő életigazságot valljuk: jobb a problémát megelőzni
                                        évi egyszeri tisztítással.</p>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer/footer.jsp" %>
        <%@include file="login/login.jsp" %>
    </body>
</html>