/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobilephoneapplication.forgotPasswordCases;

import hu.braininghub.mobileserviceapplication.forgotPasswordCases.NewPassword;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author levsz
 */
@ExtendWith(MockitoExtension.class)
public class NewPasswordTest {

    private NewPassword test;

    @BeforeEach
    void init() {
        test = new NewPassword();
    }

    private char[] generateNewPasswordAsCharArray() {
        String pw = test.generateNewPassword();
        return pw.toCharArray();
    }

    @Test
    public void testGenerateNewPasswordHasSixDigits() {
        assertEquals(generateNewPasswordAsCharArray().length, 6);
    }

    @Test
    public void testGenerateNewPasswordLastThreeDigitsAreNumbers() {
        char[] ar = generateNewPasswordAsCharArray();
        char fourth = ar[3];
        char fifth = ar[4];
        char sixth = ar[5];
        int nfourth = fourth;
        int nfifth = fifth;
        int nsixth = sixth;
        assertEquals((int) fourth, nfourth);
        assertEquals((int) fifth, nfifth);
        assertEquals((int) sixth, nsixth);
    }

}
