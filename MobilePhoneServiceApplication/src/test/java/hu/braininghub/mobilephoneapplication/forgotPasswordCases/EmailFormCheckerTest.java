/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobilephoneapplication.forgotPasswordCases;

import hu.braininghub.mobileserviceapplication.forgotPasswordCases.EmailFormChecker;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author levsz
 */
@ExtendWith(MockitoExtension.class)
public class EmailFormCheckerTest {
    
    private String testFalse1;
    private String testFalse2;
    private String testTrue1;
    private String testTrue2;
    
    @BeforeEach
    void inint() {
        testFalse1 = "proba.smtg.com";
        testFalse2 = "proba@33a.3com";
        testTrue1 = "proba@gmail.com";
        testTrue2 = "proba@freemail.com";
    }
    
    @Test
    void testHasEmailCorrectFormWithoutWorm() {
        assertEquals(EmailFormChecker.hasEmailCorrectForm(testFalse1), false);
    }
    
    @Test
    void testHasEmailCorrectFormIncludingNumberAfterWorm() {
        assertEquals(EmailFormChecker.hasEmailCorrectForm(testFalse2), false);
    }
    
    @Test
    void testHasEmailCorrectFormWithFreemail() {
        assertEquals(EmailFormChecker.hasEmailCorrectForm(testTrue1), true);
    }
    
    @Test
    void testHasEmailCorrectFormWithGmail() {
        assertEquals(EmailFormChecker.hasEmailCorrectForm(testTrue2), true);
    }
    
}
