/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service;

import hu.braininghub.mobileserviceapplication.service.feeservice.FeeService;
import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.mapper.FeeMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.feeentity.Fee;
import hu.braininghub.mobileserviceapplication.repository.fee.FeeDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author momate
 */
@ExtendWith(MockitoExtension.class)
public class FeeServiceTest {

    @Mock
    private FeeDao dao;
    @Mock
    private FeeDto dto;

    private FeeService underTest;

    private FeeMapper feeMapper;
    private Fee fee;

    @BeforeEach
    void init() {
        underTest = new FeeService();
        underTest.setFeeDao(dao);
        underTest.setFeeMapper(feeMapper);

        fee = new Fee();
        fee.setFeeName("test");
        fee.setPrice("1000");

        dto = new FeeDto();
        dto.setFeeName("test");
        dto.setPrice("1000");
    }

    @Test
    void testGetFeesWithEmptyResult() {
        //Given
        when(dao.findAll()).thenReturn(new ArrayList<>());
        //When
        List<FeeDto> fees = underTest.listFeeDto();
        //Then
        assertEquals(new ArrayList<>(), fees);
    }
    
    @Test
    void testAddFee (){
        
        List<Fee> test = Arrays.asList(fee);
        when(dao.findAll()).thenReturn(test);
        
        List<Fee> fees = dao.findAll();
        assertEquals(dto.getFeeId(),fees.get(0).getFeeId());
    }

    @Test
    void testGetFeesWith1Element() {
        //Given
        FeeDto f = new FeeDto(1, "teszt", "24234");
        //When
        List<FeeDto> fees = underTest.listFeeDto();
        fees.add(f);
        //Then
        assertEquals(Arrays.asList(f), fees);
    }

    @Test
    void testGetFeesCheckedDuplicationElements() {
        //Given
        List<FeeDto> fees = underTest.listFeeDto();
        Set<FeeDto> feeSet = fees.stream().collect(Collectors.toSet());
        //Then
        assertEquals(fees.size(), feeSet.size());

    }

}
