/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service.worksheet;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.mapper.WorkSheetMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import hu.braininghub.mobileserviceapplication.repository.user.UserDao;
import hu.braininghub.mobileserviceapplication.repository.worksheet.*;
import hu.braininghub.mobileserviceapplication.service.worksheetservice.WorkSheetService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */
@ExtendWith(MockitoExtension.class)
public class WorkSheetServiceTest {

    @Mock
    private WorkSheetDao worksheetDao;
    
    @Mock
    private UserDao userDao;
    
    @Mock
    private WorkSheetMapper worksheetMapper;
    
    private WorkSheetService underTest;
    
    private WorkSheet worksheet;
    private WorkSheetDto worksheetDto;
    private User userEntity;
    
    @BeforeEach
    void init(){
        underTest = new WorkSheetService();
        underTest.setUserDao(userDao);
        underTest.setWorkSheetDao(worksheetDao);
        underTest.setWorkSheetMapper(worksheetMapper);
        
        worksheet = new WorkSheet();
        worksheet.setImeiNumber(1L);
        worksheet.setImgPath("test");
        worksheet.setIssueDescription("test");
        worksheet.setPhoneType("test");
        worksheet.setTicketCreationDate("2020");
        worksheet.setUserId(new User());
        worksheet.setWorkSheetId(1);
        worksheet.setWorksheetStatus(WorkSheetStatus.DONE);
        
        worksheetDto = new WorkSheetDto();
        worksheetDto.setImeiNumber(1L);
        worksheetDto.setImgPath("test");
        worksheetDto.setIssueDescription("test");
        worksheetDto.setPhoneType("test");
        worksheetDto.setTicketCreationDate("2020");
        worksheetDto.setUser(new UserDto());
        worksheetDto.setWorkSheetId(1);
        worksheetDto.setWorksheetStatus(WorkSheetStatus.DONE);
    }
    
    @Test
    public void testListWorksheets(){
        WorkSheet entity = mock(WorkSheet.class);
        WorkSheetDto dto = mock(WorkSheetDto.class);
        
        when(worksheetDao.findAll()).thenReturn(Arrays.asList(entity));
        when(worksheetMapper.toDto(entity)).thenReturn(dto);
        
        List<WorkSheetDto> result = underTest.listWorkSheets();
        Assertions.assertEquals(Arrays.asList(dto), result);
    }
    
    @Test
    public void testFindWorksheetByIdWithCorrectId(){
        WorkSheet entity = mock(WorkSheet.class);
        WorkSheetDto dto = mock(WorkSheetDto.class);
        
        when(worksheetDao.findById(1)).thenReturn(Optional.of(entity));
        when(worksheetMapper.toDto(entity)).thenReturn(dto);
        
        WorkSheetDto result = underTest.findWorksheetById(1);
        Assertions.assertEquals(result, dto);
    }
    
    @Test
    public void testListWorksheetsByMailIdWithCorrectInput(){  
        String mail = "test";
        User userEntity = mock(User.class);
        
        when(userDao.findByEmail(mail)).thenReturn(Optional.of(userEntity));
        when(worksheetDao.findByUserId(userEntity.getUserId())).thenReturn(new ArrayList<>());
        
        List<WorkSheetDto> result = underTest.listWorksheetsByMail(mail);
        Assertions.assertEquals(result, new ArrayList<>());
    }
    
    @Test
    public void testListWorksheetsByMailIdWithInCorrectInput(){        
        String mail = null;
        User userEntity = mock(User.class);
        
        when(userDao.findByEmail(mail)).thenReturn(Optional.of(userEntity));
        when(worksheetDao.findByUserId(userEntity.getUserId())).thenReturn(new ArrayList<>());
        
        List<WorkSheetDto> result = underTest.listWorksheetsByMail(mail);
        Assertions.assertEquals(result, new ArrayList<>());
    }
    
    @Test
    public void addWorkSheetTest(){
        
        List<WorkSheet> test = Arrays.asList(worksheet);
        when(worksheetDao.findAll()).thenReturn(test);

        List<WorkSheet> worksheets = worksheetDao.findAll();
        assertEquals(worksheetDto.getWorkSheetId(), worksheets.get(0).getWorkSheetId());
        
    }
}
