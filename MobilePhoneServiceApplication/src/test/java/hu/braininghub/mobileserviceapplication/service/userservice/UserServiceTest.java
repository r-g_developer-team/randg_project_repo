/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.service.userservice;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.exceptions.EmailIsAlreadyExistException;
import hu.braininghub.mobileserviceapplication.mapper.UserMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import static org.junit.jupiter.api.Assertions.*;
import hu.braininghub.mobileserviceapplication.repository.user.UserDao;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author momate
 */
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private UserService underTest;
    private UserDto userDto;
    private User user;
    
    @Mock
    private UserMapper userMapper;

    @Mock
    private UserDao userDao;

    @BeforeEach
    void init() {
        underTest = new UserService();
        underTest.setUserDao(userDao);
        underTest.setUserMapper(userMapper);
        userDto = new UserDto("Molnár", "Máté", "proba@gmail.com", "1231313", "123123", "budapest", "rootroot");
        user = new User("Molnár", "Máté", "proba@gmail.com", "1231313", "123123", "budapest", "rootroot");

    }

    @Test
    void testAddUser() {

        List<User> test = Arrays.asList(user);
        when(userDao.findAll()).thenReturn(test);

        List<User> users = userDao.findAll();
        assertEquals(userDto.getFirstName(), users.get(0).getFirstName());
    }

    @Test
    void testAddUserWithExistingEmailAddress() {
        String email = user.getEmail();
        when(userDao.findByEmail(email)).thenReturn(Optional.of(user));
        assertThrows(EmailIsAlreadyExistException.class, () -> underTest.addUser(userDto));
    }

    @Test
    void testIsEmailExist() {
        String email = "proba@gmail.com";
        when(userDao.findByEmail(email)).thenReturn(Optional.of(user));
        userDao.save(user);
        assertEquals(true, underTest.isEmailExist(userDto));
    }

    @Test
    void testIsEmailExistWithNonExistingEamil() {

        assertEquals(false, underTest.isEmailExist(userDto));
    }
    
    @Test
    void testPasswordSha256() throws NoSuchAlgorithmException{
        String pw = "probaproba";
        assertNotEquals(pw, underTest.passwordSha256(pw));
    }
    
    @Test
    void testFindUserDtoByEmail() {
        String email = "proba@gmail.com";
        Optional<User> optionalUser = Optional.of(user);
        when(userDao.findByEmail(email)).thenReturn(optionalUser);
        when(userMapper.toDto(optionalUser.get())).thenReturn(userDto);
        assertEquals(underTest.findUserDtoByEmail(email), Optional.of(userDto));
    }
    
    @Test
    void testFindUserDtoByEmailWithNotExistingEmail() {
        String email = "rosszproba@gmail.com";
        Optional<User> optionalUser = Optional.empty();
        when(userDao.findByEmail(email)).thenReturn(optionalUser);
        assertEquals(underTest.findUserDtoByEmail(email), Optional.empty());
    }
    

}
