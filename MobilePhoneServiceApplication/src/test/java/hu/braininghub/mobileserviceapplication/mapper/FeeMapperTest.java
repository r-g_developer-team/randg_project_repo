package hu.braininghub.mobileserviceapplication.mapper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import hu.braininghub.mobileserviceapplication.dto.feedto.FeeDto;
import hu.braininghub.mobileserviceapplication.mapper.FeeMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.feeentity.Fee;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Leo
 */
@ExtendWith(MockitoExtension.class)
public class FeeMapperTest {

    private Fee entity;
    private FeeDto dto;

    @InjectMocks
    private FeeMapper underTest;

    @BeforeEach
    void init() {
        entity = new Fee();
        entity.setFeeName("Cleaning");
        entity.setPrice("1000");

        dto = new FeeDto();
        dto.setFeeName("Cleaning");
        dto.setPrice("1000");
    }

    @Test
    public void testGetFeeEntityWithGivenFeeDto() {

        Fee result = underTest.toEntity(dto);
        assertEquals(entity, result);
    }

    @Test
    public void testGetFeeDtoWithGivenFeeEntity() {

        FeeDto result = underTest.toDto(entity);
        assertEquals(dto, result);
    }
}
