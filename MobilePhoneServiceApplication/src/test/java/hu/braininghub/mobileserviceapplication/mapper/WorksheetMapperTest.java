/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

import hu.braininghub.mobileserviceapplication.dto.worksheetdto.WorkSheetDto;
import hu.braininghub.mobileserviceapplication.mapper.WorkSheetMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.WorkSheetStatus;
import hu.braininghub.mobileserviceapplication.repository.entity.worksheetentity.WorkSheet;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author george
 */

@ExtendWith(MockitoExtension.class)
public class WorksheetMapperTest {
    
    private WorkSheet entity;
    private WorkSheetDto dto;
    
    @InjectMocks
    private WorkSheetMapper underTest;
    
    @BeforeEach
    void init(){
    
        entity = new WorkSheet();
        entity.setImeiNumber(12345L);
        entity.setImgPath("image of phone");
        entity.setIssueDescription("decription of phone");
        entity.setPhoneType("phonetype");
        entity.setTicketCreationDate("2020");
        entity.setWorkSheetId(2);
        entity.setWorksheetStatus(WorkSheetStatus.DONE);
        
        dto = new WorkSheetDto();
        dto.setImeiNumber(12345L);
        dto.setImgPath("image of phone");
        dto.setIssueDescription("decription of phone");
        dto.setPhoneType("phonetype");
        dto.setTicketCreationDate("2020");
        dto.setWorkSheetId(2);
        dto.setWorksheetStatus(WorkSheetStatus.DONE);
    }
    
    @Test
    public void testGetWorksheetEntityWithWorksheetDtoInput(){
        
        WorkSheet result = underTest.toEntity(dto);
        
        assertEquals(entity, result);
    }
    
    @Test
    public void testGetWorksheetDtoWithWorksheetEntityInput(){
        
        WorkSheetDto result = underTest.toDto(entity);
        
        assertEquals(dto, result);
    }
}
