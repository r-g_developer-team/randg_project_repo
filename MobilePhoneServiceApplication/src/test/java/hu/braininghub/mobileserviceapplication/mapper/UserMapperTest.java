/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.mobileserviceapplication.mapper;

import hu.braininghub.mobileserviceapplication.dto.userdto.UserDto;
import hu.braininghub.mobileserviceapplication.mapper.UserMapper;
import hu.braininghub.mobileserviceapplication.repository.entity.userentity.User;
import hu.braininghub.mobileserviceapplication.service.userservice.UserService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author levsz
 */
@ExtendWith(MockitoExtension.class)
public class UserMapperTest {

    private UserMapper underTest;

    @Mock
    UserService service;

    private User entity;
    private UserDto dto;

    //String firstName, String lastName, String email, String phoneNumber, String postalCode, String city, String password
    @BeforeEach
    void init() {
        underTest = new UserMapper();
        entity = new User("firstName", "lastName", "proba@dreammail.com", "69", "8175", "Big City", "pwd");
        dto = new UserDto("firstName", "lastName", "proba@dreammail.com", "69", "8175", "Big City", "pwd", false);
    }

    @Test
    public void testToEntity() {
        assertEquals(underTest.toEntity(dto), entity);
    }

//    @Test
//    public void testToDto() {
//        Mockito.when(service.usersAndRoles(entity)).thenReturn(Boolean.FALSE);
//        
//        assertEquals(underTest.toDto(entity), dto);
//    }

}
